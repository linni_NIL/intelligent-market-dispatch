package team.id.market;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

@SpringBootApplication
@EnableScheduling
public class MainApplication {
    private static Logger logger = LoggerFactory.getLogger(MainApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        ConfigurableApplicationContext application = SpringApplication.run(MainApplication.class, args);
        Environment env = application.getEnvironment();
        String contextPath = env.getProperty("server.servlet.context-path");
        contextPath = Optional.ofNullable(contextPath).orElse("").replaceFirst("/", "");
        contextPath = (contextPath.length() <= 0 || contextPath.endsWith("/")) ? contextPath : contextPath + "/";
        String hostAddress = InetAddress.getLocalHost().getHostAddress();
        String serverPort = env.getProperty("server.port");
        String urlCtx = hostAddress + ":" + serverPort + "/" + contextPath;
        logger.info("\n----------------------------------------------------------\n\t" +
                        "\t访问地址：http://{}\n" +
                        "----------------------------------------------------------",
                 urlCtx
        );
    }
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
