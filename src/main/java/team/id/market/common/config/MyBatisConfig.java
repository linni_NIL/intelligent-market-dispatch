package team.id.market.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 431587
 * @date 2023/8/24 15:47
 */

@Configuration
public class MyBatisConfig {
    /**
     * mybatis 自定义拦截器(特殊字符处理)
     * @return
     */
    @Bean
    public EscapeInterceptor getEscapeInterceptor(){
        EscapeInterceptor interceptor = new EscapeInterceptor();
        return interceptor;
    }


}
