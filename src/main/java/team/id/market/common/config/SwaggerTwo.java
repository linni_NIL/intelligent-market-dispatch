package team.id.market.common.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Map;


/**
 * Description: swagger 配置
 */
@EnableSwagger2
@Configuration
public class SwaggerTwo {

    private static final String S_TITLE = "二代agv智能调度系统项目 API文档";
    private static final String S_DESCRIPTION = "在这最美丽的年龄，尽情奔跑吧";
    private static final String S_URL = "server";
    private static final String S_VERSION = "1.0";




    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(S_TITLE)
                .description(S_DESCRIPTION)
                .termsOfServiceUrl(S_URL)
                .version(S_VERSION).build();
    }


    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .directModelSubstitute(Map.class, Object.class);// 全局配置，解决swagger2返回map复杂结构不能解析的问题;
    }



}
