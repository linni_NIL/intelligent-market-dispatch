package team.id.market.common.config.mybatis;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;

import lombok.NoArgsConstructor;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Like 转义 插件：<br/>
 * 在mybatis plus 配置此插件使用；mybatis plus 插件使用机制，优先使用原始参数进行条件查询。<br/>
 * 1、如果 count 记录为0 时，name将不再执行任何before query 调用； <br/>
 * 2、如果 count 结果非0 时，执行插件业务逻辑。 <br/>
 *
 * @see MybatisPlusInterceptor#intercept(org.apache.ibatis.plugin.Invocation)
 * for (InnerInterceptor query : interceptors) {
 * if (!query.willDoQuery(executor, ms, parameter, rowBounds, resultHandler, boundSql)) {
 * return Collections.emptyList();
 * }
 * query.beforeQuery(executor, ms, parameter, rowBounds, resultHandler, boundSql);
 * }
 * <p>
 * 使用方法：
 * <ol>
 * <li> 添加插件 到mybatis plus </li>
 * </ol>
 * </p>
 */
@NoArgsConstructor
public class LikeStringEscapeInterceptor implements InnerInterceptor {
    private static final Logger logger = LoggerFactory.getLogger(LikeStringEscapeInterceptor.class);

    @Override
    public void beforePrepare(StatementHandler sh, Connection connection, Integer transactionTimeout) {
        MyBatisUtil.escapeParameterIfContainingLike(sh.getBoundSql());
    }

    @Override
    public void beforeQuery(Executor executor, MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        // 到此，说明mybatis plus 预执行有结果。
        logger.debug("LikeStringEscapeInterceptor beforeQuery");
        SqlCommandType sqlCommandType = ms.getSqlCommandType();
        StatementType statementType = ms.getStatementType();
        // 只处理 有参数的查询语句
        if (sqlCommandType == SqlCommandType.SELECT && statementType == StatementType.PREPARED) {
            MyBatisUtil.escapeParameterIfContainingLike(boundSql);
        }
    }
}

