package team.id.market.common.config.mybatis;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
* SQL相关工具类，模糊查询拦截后逻辑处理
* @creator zsm
* @create 2023-3-4
*/
@Slf4j
@NoArgsConstructor
public class MyBatisUtil {

    /**
     * 检查sql中，是否含有like查询
     */
    public static final Pattern LIKE_PARAM_PATTERN = Pattern.compile("like\\s(concat)?[\\w'\"\\(\\)\\%,\\s\\n\\t]*\\?", Pattern.CASE_INSENSITIVE);

    public static void escapeParameterIfContainingLike(BoundSql boundSql) {
        if (boundSql == null) {
            return;
        }
        String prepareSql = boundSql.getSql();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();

        // 找到 like 后面的参数
        List<Integer> position = findLikeParam(prepareSql);
        if (position.isEmpty()) {
            return;
        }

        List<ParameterMapping> likeParameterMappings = new ArrayList<>(parameterMappings.size());

        // 复制
        MetaObject metaObject = SystemMetaObject.forObject(boundSql.getParameterObject());
        for (ParameterMapping m : parameterMappings) {
            if (!metaObject.hasGetter(m.getProperty())) {
                continue;
            }
            boundSql.setAdditionalParameter(m.getProperty(), metaObject.getValue(m.getProperty()));
        }

        for (int i = 0; i < position.size(); i++) {
            ParameterMapping parameterMapping = parameterMappings.get(position.get(i));
            likeParameterMappings.add(parameterMapping);
        }
        // 覆盖 转义字符
        delegateMetaParameterForEscape(boundSql, likeParameterMappings);
    }

    /**
     * @param boundSql 原 boundSql
     * @param likeParameterMappings 需要转义的参数
     * @return 支持转义的 boundSql
     */
    public static void delegateMetaParameterForEscape(
            BoundSql boundSql,
            List<ParameterMapping> likeParameterMappings) {

        log.debug("like String Escape parsing ...");

        MetaObject metaObject = SystemMetaObject.forObject(boundSql.getParameterObject());

        for (ParameterMapping mapping : likeParameterMappings) {

            String property = mapping.getProperty();

            if (!metaObject.hasGetter(property)) {
                continue;
            }

            Object value = metaObject.getValue(property);
            if (value instanceof String) {
                boundSql.setAdditionalParameter(property, convertToSQLSafeValue((String) value,property));
            }
        }
    }

    /**
     * 匹配like 位置， 如
     * like concat('%',?,'%'）
     * like CONCAT('%',?,'%'）
     * LIKE CONCAT('%', ?,'%'）
     * lIKE conCAT('%', ?,'%'）
     * like ?
     * @param prepareSql
     * @return
     */
    public static List<Integer> findLikeParam(String prepareSql) {

        if (StringUtils.isEmpty(prepareSql)) {
            return Collections.emptyList();
        }
        Matcher matcher = LIKE_PARAM_PATTERN.matcher(prepareSql);

        if (!matcher.find()) {
            return Collections.emptyList();
        }

        matcher.reset();
        int pos = 0;
        List<Integer> indexes = new ArrayList<>();
        while (matcher.find(pos)) {
            int start = matcher.start();
            int index = StringUtils.countMatches(prepareSql.substring(0, start), "?");
            indexes.add(index);
            pos = matcher.end();
        }
        return indexes;
    }


    /**
     * MySQL需要转义的字段：\ % _
     */
    public static final Pattern PATTERN_MYSQL_ESCAPE_CHARS = Pattern.compile("(['_%\\\\]{1})");

    /**
     * 在SQL进行like时使用 ，mysql like时，参数使用传值 SqlUtils.convertToSQLSafeValue(String)； 禁止与escape 同时使用。
     *
     * 转义mysql的特殊字符 包括 '\', '%', '_', ''',
     * @param str
     * @return 返回可能为null eg:
     *  1'2_3%4\ 5 ?\ 转义后  1\'2\_3\%4\\\\ 5 ?\\\\
     *  null >> null
     *  """ >> ""
     *  "%" >> "\%"
     *  "\" >> "\\\\\"
     *  "_" >> "\_"
     *  "_%" >> "\_\%"
     */
    public static String convertToSQLSafeValue(String str,String property) {
        return SqlUtils.convertToSQLSafeValue(str,property);
    }
}
