package team.id.market.common.config.mybatis;


import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import org.mybatis.spring.annotation.MapperScan;

/**
 * MyBatis配置数据源、sqlSessionFactory、事务管理器、分页插件、自定义拦截器等
 * @creator zsm
 * @create 2023-3-4
 */
@Configuration
@MapperScan(sqlSessionFactoryRef = "masterSqlSessionFactory")
public class MybatisPlusConfig {

    @Autowired
    private MybatisPlusProperties properties;
    // 使用MyBatis中的分页插件

    // 自定义拦截器实现模糊查询中的特殊字符处理
    @Bean
    public LikeStringEscapeInterceptor myInterceptor() {
        LikeStringEscapeInterceptor sql = new LikeStringEscapeInterceptor();
        return sql;
    }
    // 数据源配置
    @Bean(name = "masterDataSource")
    @ConfigurationProperties("spring.datasource")
    @Primary
    public DataSource masterDataSource() {
        return DruidDataSourceBuilder.create().build();
    }
    // 配置sqlSessionFactory
    @Bean(name = "masterSqlSessionFactory")
    @Primary
    public SqlSessionFactory masterSqlSessionFactory(@Qualifier("masterDataSource") DataSource dataSource,
                                                     MybatisPlusInterceptor paginationInterceptor, LikeStringEscapeInterceptor myInterceptor) throws Exception {
        final MybatisSqlSessionFactoryBean sessionFactory = new MybatisSqlSessionFactoryBean();

        sessionFactory.setDataSource(dataSource);

        if (this.properties.getConfigurationProperties() != null) {
            sessionFactory.setConfigurationProperties(this.properties.getConfigurationProperties());
        }
        GlobalConfig globalConfig = this.properties.getGlobalConfig();
        sessionFactory.setGlobalConfig(globalConfig);

        paginationInterceptor.addInnerInterceptor(myInterceptor);
        List<Interceptor> interceptors = new ArrayList<>();
        interceptors.add(paginationInterceptor);
        sessionFactory.setPlugins(interceptors.toArray(new Interceptor[1]));

        return sessionFactory.getObject();
    }
    @Bean(name = "masterTransactionManager")
    public DataSourceTransactionManager masterTransactionManager(@Qualifier("masterDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }


    /**
     * 分页插件
     */
    @Bean
    public MybatisPlusInterceptor paginationInterceptor() {

        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }


}




