package team.id.market.common.config.mybatis;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.regex.Pattern;

/**
 * SQL相关工具类,处理转义字符
 * @creator zsm
 * @create 2023-3-4
 */
public class SqlUtils {

    private SqlUtils() {
    }

    /**
     * 在SQL进行like时使用 ，mysql like时，参数使用传值 SqlUtils.convertToSQLSafeValue(String)； 禁止与escape 同时使用。
     *
     * 转义mysql的特殊字符 包括 '\', '%', '_', ''',
     * @param before
     * @return 返回可能为null eg:
     *  1'2_3%4\ 5 ?\ 转义后  1\'2\_3\%4\\\\ 5 ?\\\\
     *  null >> null
     *  """ >> ""
     *  "%" >> "\%"
     *  "\" >> "\\\\\"
     *  "_" >> "\_"
     *  "_%" >> "\_\%"
     */
    public static String convertToSQLSafeValue(String before,String property) {
        if(StringUtils.isNotBlank(before)){
            before = before.replaceAll("\\\\", "\\\\\\\\");
            before =before.replaceAll("'","\\\\'");
            before = before.replaceAll("_", "\\\\_");
            if (property.contains("ew.paramNameValuePairs.")){
                String substring = before.substring(1, before.length() - 1);
                before = "%"+substring.replaceAll("%", "\\\\%")+"%";
            }else {
                before = before.replaceAll("%", "\\\\%");
            }

        }
        return before ;
    }

}
