package team.id.market.common.constant;

/**
 * @author 431587
 * @date 2023/5/27 17:12
 */
public class FaultState {

    public static final String CREATE = "CREATE";

    public static final String SOLVE = "SOLVE";
}
