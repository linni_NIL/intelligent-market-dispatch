package team.id.market.common.constant;

/**
 * @author 431587
 * @date 2023/5/27 17:07
 */
public class OrderState {
    public static final String RAW = "RAW";
    public static final String ACTIVE = "ACTIVE";
    public static final String DISPATCHABLE = "DISPATCHABLE";
    public static final String CREATE = "CREATE";

    public static final String BEING_PROCESSED = "BEING_PROCESSED";

    public static final String FINISHED = "FINISHED";

    public static final String WITHDRAWN = "WITHDRAWN";

    public static final String UNROUTABLE = "UNROUTABLE";

    public static final String FAILED = "FAILED";

    public static final String DRIVER_ORDER_INDEX = "DRIVER_ORDER_INDEX";
}
