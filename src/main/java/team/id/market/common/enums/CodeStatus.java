package team.id.market.common.enums;


import team.id.market.common.exception.MessageException;

/**
 * @author 431587
 * @date 2023/4/18 15:07
 */
public interface CodeStatus {
    default Integer status() {
        throw new MessageException(300, "状态码不能为空");
    }


    default String msg() {
        throw new MessageException(300, "内容不能为空");
    }
}
