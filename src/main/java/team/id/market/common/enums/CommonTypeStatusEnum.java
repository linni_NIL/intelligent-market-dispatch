package team.id.market.common.enums;


import lombok.Getter;

/**
 * @author 260654
 * @create 2023/5/9 10:39
 */
@Getter
public enum CommonTypeStatusEnum {
    //任务状态
    TASK_STATUS_TO_BE_EXECUTED(1,"待执行"),TASK_STATUS_EXECUTING(2,"执行中"),TASK_STATUS_FINISHED(3,"已完成"),
    TASK_STATUS_TERMINATED(4,"已终止"),TASK_STATUS_NO_ROUTE(5,"无法执行"),
    //任务优先级
    TASK_PRIORITY_LOW(3600,"低"),TASK_PRIORITY_MIDDLE(3000,"中"),TASK_PRIORITY_HIGH(2400,"高"),
    TASK_PRIORITY_URGENT(1800,"紧急"),
    //任务动作内核
    TASK_OPERATION_MOVE(0,"MOVE"),TASK_OPERATION_LOAD(7,"loopback:loadOperation"),TASK_OPERATION_UNLOAD(8,"loopback:unloadOperation"),
    TASK_OPERATION_KERNEL_CHARGE(10,"CHARGE"),
    //任务动作后台
    TASK_OPERATION_NOP(0,""),TASK_OPERATION_STAND_BY_LIFT(7,"顶升"),TASK_OPERATION_STAND_BY_DECLINE(8,"下降"),
    TASK_OPERATION_CHARGE(10,"充电"),
    //错误等级
    ERROR_LEVEL_TEN(10,""),ERROR_LEVEL_FIFTEEN(15,""),ERROR_LEVEL_EIGHTEEN(18,""),ERROR_LEVEL_TWENTY(20,""),ERROR_LEVEL_THIRTY(30,""),
    ;
    private final Integer code;

    private final String msg;


    CommonTypeStatusEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }


}
