package team.id.market.common.excel;

/**
 * 关于poi的枚举
 */
public enum ExcelEnum {

    IMPORT,
    EXPORT,
    IMPORT_ADN_EXPORT;

}
