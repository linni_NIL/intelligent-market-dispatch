package team.id.market.common.excel;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * desc: 实体导入导出类 注解
 * ps 该功能只适用于简单类型的导入与导出，复杂的暂且未做。
 * Created by achims on 2019/9/26.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ImportAndExport {

    /**
     * excel对应的文件头描述 需要在实体类与excel进行映射，即将实体类上的字段添加该注解 desc则表示excel文件头对应的数据内容
     *
     * @return
     */
    String desc() default "";//添加说明
    String patternMessage() default "";//正则提示语

    boolean isNeed() default false;//表示读取的时候当前数据是否必须填写，解决某些单元格为空的情况

    String pattern() default "";//传入数据的正则格式，确保数据是在其值

    String[] limitValue() default {};//限制传入的数据内容

    ExcelEnum exportOrImport() default ExcelEnum.IMPORT_ADN_EXPORT;//是导入还是导出需要，还是导入导出都需要，默认为导入导出都需要


    String format() default "";//数据的转换 目前没有使用

    int length() default 0;
    String lengthMessage() default "";
    /**
     * 如数字或者字母 转换成中文
     * <p>
     * <p>
     * 如 2_成功 1_失败，返回的就是成功 或者失败
     *
     * @return
     */
    String[] valueTransfer() default {};//数据的转换 目前没有使用

    int max() default 2147483647;

    int min() default 0;
}
