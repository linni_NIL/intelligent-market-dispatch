package team.id.market.common.excel;

/**
 * 导出数据的格式转换
 */
public @interface ValueTransfer {

    /**
     * 针对的是date类型数据的导出
     *
     * @return
     */
    String pattern() default "yyyy-MM-dd HH:mm:ss";

    /**
     * 针对的是枚举类型的数据转换
     */
    String[] number() default {};// 对应的数字

    /**
     *
     */
    String[] numberValue() default {};//对应数据的含义
}
