package team.id.market.common.exception;

import cn.hutool.json.JSONUtil;
import team.id.market.common.enums.CodeStatus;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 日志记录异常  ES g
 *
 * @program:ontheway_backend
 * @description:
 * @author:achims
 * @create:2021-03-10 10-25
 **/
@Data
@NoArgsConstructor
@Slf4j
public class ExceptionLogDto {

    private Integer status;//


    private boolean printTrace = true;

    private String msg;


    private Exception exception;

    private StackTraceElement stack_trace;


    /**
     *
     */
    public static void exceptionPrint(Integer status, String msg, Exception exception) {
        exception.printStackTrace();
        ExceptionLogDto exceptionLogDto = new ExceptionLogDto();
        exceptionLogDto.setStatus(status);
        exceptionLogDto.setMsg(msg);
        exceptionLogDto.setException(exception);
        if (exception.getStackTrace() != null && exception.getStackTrace().length > 0) {
            exceptionLogDto.setStack_trace(exception.getStackTrace()[0]);
        }
        log.error(JSONUtil.toJsonStr(exceptionLogDto));
    }


    public static void exceptionPrint(MessageException exception) {
        exception.printStackTrace();
        ExceptionLogDto exceptionLogDto = new ExceptionLogDto();
        exceptionLogDto.setStatus((int) exception.getStatus());
        exceptionLogDto.setMsg(exception.getMsg());
//        exceptionLogDto.setException(exception);
//        if (exception.getStackTrace() != null && exception.getStackTrace().length > 0) {
//            exceptionLogDto.setStack_trace(exception.getStackTrace()[0]);
//        }
        log.error(JSONUtil.toJsonStr(exceptionLogDto));
    }

    /**
     *
     */
    public static void exceptionPrint(CodeStatus codeStatus, Exception exception) {
        exception.printStackTrace();
        ExceptionLogDto exceptionLogDto = new ExceptionLogDto();
        exceptionLogDto.setStatus(codeStatus.status());
        exceptionLogDto.setMsg(codeStatus.msg());
//        exceptionLogDto.setException(exception);
//        if (exception.getStackTrace() != null && exception.getStackTrace().length > 0) {
//            exceptionLogDto.setStack_trace(exception.getStackTrace()[0]);
//        }
        log.error(JSONUtil.toJsonStr(exceptionLogDto));
    }
}
