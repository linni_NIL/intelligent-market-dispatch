package team.id.market.common.exception;


import team.id.market.common.enums.CodeEnum;

import team.id.market.common.result.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MultipartException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.UnsupportedEncodingException;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * 这里局异常，除了参数异常走的是AOP之外，所有异常都会经过当前异常类
 *
 * @author achims
 */
@RestControllerAdvice
@Slf4j
@Order(Ordered.LOWEST_PRECEDENCE)
public class GlobalExceptionHandler {
    //
//
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    //所有错误异常
    @ExceptionHandler(value = Exception.class)
    public CommonResult globalExceptionHandler(Exception exception) {
//        exception.printStackTrace();
        ExceptionLogDto.exceptionPrint(CodeEnum.SYSTEM_ERROR, exception);
        logger.warn("系统错误"+exception.getMessage());
        return CommonResult.failed(CodeEnum.SYSTEM_ERROR);
    }


    //
    //自定义异常
    @ExceptionHandler(value = MessageException.class)
    public CommonResult messageExceptionHandler(Exception exception) {
        MessageException messageException = (MessageException) exception;
        ExceptionLogDto.exceptionPrint(messageException);
        logger.warn("自定义错误"+exception.getMessage());
        return CommonResult.failed(messageException);
    }

//    //自定义异常
//    @ExceptionHandler(value = MyException.class)
//    public CommonResult myExceptionHandler(Exception exception) {
//        MyException myException = (MyException) exception;
//        ExceptionLogDto.exceptionPrint(myException);
//        return CommonResult.failed(myException);
//    }

    //
//    //spring -validator
    @ExceptionHandler(value = ConstraintViolationException.class)
    public CommonResult constraintViolationException(Exception exception) {
        ConstraintViolationException exception1 = (ConstraintViolationException) exception;
        Set<ConstraintViolation<?>> set = exception1.getConstraintViolations();
        List<ConstraintViolation> constraintViolations = new ArrayList<>(set);
        if (!constraintViolations.isEmpty()) {
            return CommonResult.failed(CodeEnum.GL10003, constraintViolations.get(0).getMessage());
        }
        ExceptionLogDto.exceptionPrint(CodeEnum.GL10003, exception);
        return CommonResult.failed(CodeEnum.SYSTEM_ERROR);
    }


    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public CommonResult HttpMessageNotReadableException(Exception exception) {
        ExceptionLogDto.exceptionPrint(CodeEnum.REQUEST_ERROR, exception);
        return CommonResult.failed(CodeEnum.REQUEST_ERROR.status(),exception.getMessage());
    }


    @ExceptionHandler(value = UnsupportedEncodingException.class)
    public CommonResult UnsupportedEncodingException(Exception exception) {
        ExceptionLogDto.exceptionPrint(CodeEnum.REQUEST_ERROR, exception);
        return CommonResult.failed(CodeEnum.REQUEST_ERROR.status(),exception.getMessage());
    }


    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public CommonResult methodArgumentNotValidException(Exception exception) {
        ExceptionLogDto.exceptionPrint(CodeEnum.REQUEST_ERROR, exception);
        MethodArgumentNotValidException methodArgumentNotValidException = (MethodArgumentNotValidException) exception;
        //按需重新封装需要返回的错误信息
        List<FieldError> fieldErrors = methodArgumentNotValidException.getBindingResult().getFieldErrors();
        if (!fieldErrors.isEmpty()) {
            return CommonResult.failed(CodeEnum.REQUEST_ERROR.status(), fieldErrors.get(0).getDefaultMessage());
        }
        return CommonResult.failed(CodeEnum.REQUEST_ERROR.status(),exception.getMessage());
    }

    /**
     * 文件上传大小限制
     * @param exception
     * @return
     */
    @ExceptionHandler(value = MultipartException.class)
    public CommonResult fileUploadExceptionHandler(MultipartException exception) {
        ExceptionLogDto.exceptionPrint(CodeEnum.GL31011, exception);
//        if (exception.getRootCause() instanceof FileUploadBase.FileSizeLimitExceededException) {
//            CommonResult.failed(CodeEnum.GL31011.status(),exception.getMessage());
//        }else if(exception.getRootCause() instanceof FileUploadBase.SizeLimitExceededException){
//            CommonResult.failed(CodeEnum.GL31011.status(),exception.getMessage());
//        }else {
//            CommonResult.failed(CodeEnum.GL31011.status(),exception.getMessage());
//        }
        return CommonResult.failed(CodeEnum.GL31011.status(),"文件大小不能超过20M，请检查后重新导入。");
    }

    /**
     * 时间格式转换错误
     */
    @ExceptionHandler(value =DateTimeParseException.class)
    public CommonResult dateTimeParseExceptionHandler(DateTimeParseException exception){
        ExceptionLogDto.exceptionPrint(CodeEnum.GL31030, exception);
        return CommonResult.failed(CodeEnum.GL31030.status(),CodeEnum.GL31030.getMsg());
    }


}
