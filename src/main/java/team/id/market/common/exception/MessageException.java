package team.id.market.common.exception;


import team.id.market.common.enums.CodeEnum;
import team.id.market.common.enums.CodeStatus;
import team.id.market.common.result.CommonResult;

/**
 * @author 431587
 * @date 2023/4/18 15:08
 */
public class MessageException extends RuntimeException {

    private Integer status;

    @Override
    public String toString() {
        return "MessageException{" +
                "status=" + status +
                ", errorMessage=" + errorMessage +
                ", msg='" + msg + '\'' +
                '}';
    }

    private Object errorMessage;


    private String msg;


    public MessageException(CodeStatus codeStatus, Object... args) {
        super(String.format(codeStatus.msg(), args));
        this.status = codeStatus.status();
    }

    /**
     * 错误message的反馈
     *
     * @param codeStatus
     * @param args
     */
    public MessageException(CodeStatus codeStatus, Object args) {
        super(String.format(codeStatus.msg()));
        this.status = codeStatus.status();
        this.errorMessage = args;
    }


//    /**
//     * @param codeEnum 错误枚举
//     * @param args     错误对象信息
//     */
//    public MessageException(CodeEnum codeEnum, Object... args) {
//        super(String.format(codeEnum.msg(), args));
//        this.status = codeEnum.status();
//    }

//    /**
//     * 错误message的反馈
//     *
//     * @param codeEnum
//     * @param args
//     */
//    public MessageException(CodeEnum codeEnum, Object args) {
//        super(String.format(codeEnum.msg()));
//        this.status = codeEnum.status();
//        this.errorMessage = args;
//    }

    public MessageException(Integer status, String message, Object args) {
        super(String.format(message, args));
        this.status = status;
    }

    public MessageException(Integer status, String message) {
        super(message);
        this.status = status;

    }

    public MessageException(String message) {
        super(message);
        this.status = CodeEnum.FAILED.getStatus();
    }


    public MessageException(CommonResult result) {
        super(String.format(result.getMsg(), result.getResult()));
        this.status = (int) result.getStatus();
    }

    public long getStatus() {
        return status;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
