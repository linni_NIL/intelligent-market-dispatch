package team.id.market.common.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import team.id.market.common.enums.CodeEnum;
import team.id.market.common.result.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * @program:logistics_transport_backend
 * @description:
 * @author:achims
 * @create:2020-11-24 13-55
 * 这里是spring mvc中的异常接收，ps 在gateway中一定要把这个类排除，否则无法启动
 **/

@RestControllerAdvice
@Slf4j
/**
 * 如果不设置高级别的话  那么就走上面的了
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SpringMvcExceptionHandler {


    /**
     * 忽略参数缺失异常处理器
     *
     * @param e 忽略参数异常
     * @return ResponseResult
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public CommonResult parameterMissingExceptionHandler(MissingServletRequestParameterException e) {
        log.error("", e);
        return CommonResult.failed(CodeEnum.GL10003, "请求参数 " + e.getParameterName() + " 不能为空");
    }


    /**
     * 忽略参数类型不匹配异常处理器
     *
     * @param e 忽略参数异常
     * @return ResponseResult
     */

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public CommonResult methodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        log.error("", e);
        return CommonResult.failed(CodeEnum.GL10003, "传递参数格式错误 " + e.getName());
    }




    //  ps 这四种异常在gateway中因为jar冲突导致无法使用
    //rest调用
    @ExceptionHandler(value = ResourceAccessException.class)
    public CommonResult resourceAccessException(Exception exception) {
        exception.printStackTrace();
        return CommonResult.failed(CodeEnum.FEIGEN_ERROR);
    }

    //
    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public CommonResult httpRequestMethodNotSupportedException(Exception exception) {
        exception.printStackTrace();
        return CommonResult.failed(CodeEnum.REQUEST_ERROR);
    }


    @ExceptionHandler(value = InvalidFormatException.class)
    public CommonResult InvalidFormatException(Exception exception) {
        exception.printStackTrace();
        return CommonResult.failed(CodeEnum.REQUEST_ERROR);
    }


}

