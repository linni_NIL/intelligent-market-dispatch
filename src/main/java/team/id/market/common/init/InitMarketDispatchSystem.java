package team.id.market.common.init;

import org.apache.poi.xssf.model.MapInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import team.id.market.common.jgrapht.DijkstraRouter;
import team.id.market.common.pool.ObjectPool;
import team.id.market.navigation.entity.MapDesignInfo;
import team.id.market.navigation.entity.MapPathInfo;
import team.id.market.navigation.entity.MapPointInfo;
import team.id.market.navigation.entity.simulation.MapPointInfoTO;
import team.id.market.navigation.mapper.MapDesignMapper;
import team.id.market.navigation.mapper.MapPathMapper;
import team.id.market.navigation.mapper.MapPointMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class InitMarketDispatchSystem implements ApplicationListener<ApplicationReadyEvent> {
    private static Logger logger = LoggerFactory.getLogger(InitMarketDispatchSystem.class);


    @Value("${map.building}")
    private String building;

    @Autowired
    private DijkstraRouter dijkstraRouter;

    @Autowired
    private MapDesignMapper mapDesignMapper;

    @Autowired
    private MapPointMapper mapPointMapper;

    @Autowired
    private MapPathMapper mapPathMapper;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        this.init();
    }

    public void init(){
        logger.info("====初始化=====");
        this.initMapElements();
    }

    /**
     * 静态导航实现===默认读取point表等
     *
     * 系统启动-->初始化地图元素(点、边、商铺信息等)并加入对象池里，供jgrapht生成路由
     *
     * */
    private void initMapElements(){
        List<MapDesignInfo> allDesignSerial = mapDesignMapper.getAllDesignSerial(building);
        if (!allDesignSerial.isEmpty()){

            List<MapPathInfo> pathResult = new ArrayList<>();
            List<MapPointInfo> pointResult = new ArrayList<>();
            for (MapDesignInfo mapDesignInfo : allDesignSerial) {
                List<MapPathInfo> pathInfoList = mapPathMapper.getAllMapPathBySerial(mapDesignInfo.getDesignSerial());
                List<MapPointInfo> pointInfoList = mapPointMapper.getAllMapPointBySerial(mapDesignInfo.getDesignSerial());
                pathResult.addAll(pathInfoList);
                pointResult.addAll(pointInfoList);
                ObjectPool.getFloorInfoMap().put(mapDesignInfo.getDesignSerial(),mapDesignInfo.getFloor());
            }

            ObjectPool.getPathInfoMap().put(building,pathResult);

            List<MapPointInfoTO> mapPointInfoTOS = translateToPointTO(pointResult, pathResult);
            ObjectPool.getPointInfoMap().put(building,mapPointInfoTOS);

            dijkstraRouter.initGraphElement();
        }
    }

    private List<MapPointInfoTO> translateToPointTO(List<MapPointInfo> pointList,List<MapPathInfo> pathList){
        List<MapPointInfoTO> pointResult = new ArrayList<>();
        for (MapPointInfo mapPointInfo : pointList) {
            List<MapPointInfo> linkPointList = new ArrayList<>();
            for (MapPathInfo mapPathInfo : pathList) {
                if (Objects.equals(mapPathInfo.getSrcPointName(),mapPointInfo.getPointID())){
                    MapPointInfo linkPoint = mapPointMapper.getPointByPointId(mapPathInfo.getDesPointName());
                    linkPointList.add(linkPoint);
                }
            }
            MapPointInfoTO.MapPointInfoTOBuilder pointBuilder = MapPointInfoTO.builder();
            MapPointInfoTO PointTO = pointBuilder.id(mapPointInfo.getId())
                    .pointID(mapPointInfo.getPointID())
                    .hashCode(mapPointInfo.getHashCode())
                    .designSerial(mapPointInfo.getDesignSerial())
                    .xLocation(mapPointInfo.getXLocation())
                    .yLocation(mapPointInfo.getYLocation())
                    .pointType(mapPointInfo.getPointType())
                    .marketId(mapPointInfo.getMarketId())
                    .creatTime(mapPointInfo.getCreatTime())
                    .updateTime(mapPointInfo.getUpdateTime())
                    .isDelete(mapPointInfo.getIsDelete())
                    .modifyUser(mapPointInfo.getModifyUser())
                    .pointDescription(mapPointInfo.getPointDescription())
                    .pointName(mapPointInfo.getPointName())
                    .outgoingPointList(linkPointList).build();

            pointResult.add(PointTO);
        }

        return pointResult;
    }
}
