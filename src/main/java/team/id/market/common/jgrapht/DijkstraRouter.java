package team.id.market.common.jgrapht;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import team.id.market.common.pool.ObjectPool;
import team.id.market.common.result.CommonResult;
import team.id.market.navigation.entity.MapPathInfo;
import team.id.market.navigation.entity.MapPointInfo;
import team.id.market.navigation.entity.dto.MapPathDto;
import team.id.market.navigation.entity.dto.MapPointDto;
import team.id.market.navigation.entity.dto.MarketInfoDto;
import team.id.market.navigation.entity.simulation.MapPointInfoTO;
import team.id.market.navigation.entity.simulation.SimulationMapPointInfo;
import team.id.market.navigation.entity.simulation.SimulationRouteInfo;
import team.id.market.navigation.entity.simulation.dto.SimulationNavigateResultDTO;
import team.id.market.navigation.mapper.MarketMapper;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author wsh
 * @date 2023/10/31 01:56
 */
@Component("DijkstraRouter")
public class DijkstraRouter {
    private static Logger logger = LoggerFactory.getLogger(DijkstraRouter.class);

    private final Map<String, Graph<String, DefaultEdge>> graphMap = new ConcurrentHashMap<>();

    @Value("${map.building}")
    private String building;

    public Map<String, Graph<String, DefaultEdge>> getGraphMap() {
        return graphMap;
    }

    @Autowired
    private MarketMapper marketMapper;

    /**
     * 初始化graph
     *
     * mapId 加载的地图id
     */
    public void initGraphElement() {
        graphMap.clear();

        Graph<String, DefaultEdge> graph = new DefaultDirectedGraph<>(DefaultEdge.class);
        List<MapPointInfoTO> pointList = ObjectPool.getPointInfoMap().get(building);
        List<MapPathInfo> pathList = ObjectPool.getPathInfoMap().get(building);

        for (MapPointInfoTO point : pointList) {
            graph.addVertex(point.getPointID());
        }

        for (MapPathInfo path : pathList) {
            graph.addEdge(path.getSrcPointName(), path.getDesPointName());
            graph.addEdge(path.getDesPointName(), path.getSrcPointName());
        }

        graphMap.put(building, graph);
    }

    /**
     * 获取算法直接输出的路线，只包含点集合
     * <p>
     * sourcePoint 起点
     * targetPoint 目标点
     * return 路线
     */
//    public List<String> getRoute(String sourcePoint, String targetPoint) {
//        if (graphMap.get(loadingMap).edgeSet().isEmpty()){
//            return null;
//        }
////       使用迪杰斯特拉最短路径算法
//        DijkstraShortestPath<String, DefaultEdge> dijkstraAlgo =
//                new DijkstraShortestPath<>(graphMap.get(loadingMap));
//
//        ShortestPathAlgorithm.SingleSourcePaths<String, DefaultEdge> algorithm = dijkstraAlgo.getPaths(sourcePoint);
//        GraphPath<String, DefaultEdge> path = algorithm.getPath(targetPoint);
//        return path.getVertexList();
//    }


/**
 *getRoutePlaning()
 * param @sourcePoint-->  pointID
 * param @targetPoint-->  pointID
 *
 * */
    public SimulationNavigateResultDTO getRoutePlaning(String sourcePoint, String targetPoint) {
        if (graphMap.get(building).edgeSet().isEmpty()){
            logger.warn("点路由初始化信息为空");
            return null;
        }
//       使用迪杰斯特拉最短路径算法
        DijkstraShortestPath<String, DefaultEdge> dijkstraAlgo =
                new DijkstraShortestPath<>(graphMap.get(building));
        try {
            ShortestPathAlgorithm.SingleSourcePaths<String, DefaultEdge> algorithm = dijkstraAlgo.getPaths(sourcePoint);
            GraphPath<String, DefaultEdge> path = algorithm.getPath(targetPoint);
            if (Objects.equals(path,null)){
                logger.error("未找到相关路线，点位异常");
                return null;
            }
            SimulationNavigateResultDTO simulationNavigateResultDTO = translateToSimulationPointList(path.getVertexList());
            return simulationNavigateResultDTO;
        }catch (Exception e){
            logger.error("获取路线异常",e);
            return null;
        }
    }

    /**
     * 将路径算法的输出结果转换具体信息(如Point类、店铺信息等)  模拟导航
     *
     * GraphPath<String, DefaultEdge>
     * return 信息集合
     * translateToObject
     * */
    public SimulationNavigateResultDTO translateToSimulationPointList(List<String> pointList){
        SimulationNavigateResultDTO simulationNavigateResultDTO = new SimulationNavigateResultDTO();
        List<SimulationMapPointInfo> mapPointDtos = new LinkedList<>();
        for (String point : pointList) {
            Optional<MapPointInfoTO> mapPoint = ObjectPool.getPointInfoMap().get(building).stream()
                    .filter(item -> Objects.equals(item.getPointID(), point)).findFirst();
            if (mapPoint.isPresent()){
                MapPointInfoTO mapPointInfo = mapPoint.get();
                mapPointDtos.add(new SimulationMapPointInfo(
                        mapPointInfo.getPointID(),
                        mapPointInfo.getPointName(),
                        ObjectPool.getFloorInfoMap().get(mapPointInfo.getDesignSerial()),
                        mapPointInfo.getXLocation(),
                        mapPointInfo.getYLocation(),
                        mapPointInfo.getPointType(),getPassingInfo(mapPointInfo)));
            }
        }

        simulationNavigateResultDTO.setSimulationMapPointInfos(mapPointDtos);
        return simulationNavigateResultDTO;
    }

    public String getPassingInfo(MapPointInfoTO mapPointInfo){

        if (!Objects.equals(mapPointInfo.getPointName(),null) &&
                !mapPointInfo.getPointName().isEmpty()){
            return mapPointInfo.getPointName();
        }else{
            List<MapPointInfo> outgoingPointList = mapPointInfo.getOutgoingPointList();
            for (MapPointInfo pointInfo : outgoingPointList) {
                if (!Objects.equals(pointInfo.getMarketId(),null)
                        &&!pointInfo.getMarketId().isEmpty()){
                  return  marketMapper.getMarketById(pointInfo.getMarketId()).getMarketNameSc();
                }
            }
        }
        return null;

    }
}
