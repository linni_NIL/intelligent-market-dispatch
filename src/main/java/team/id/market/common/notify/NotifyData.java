package team.id.market.common.notify;

import java.util.Date;

/**
 * @author 431587
 * @date 2023/6/13 15:17
 */

public class NotifyData {

private String notifyType;

private Object data;

private String date;

    public NotifyData(String notifyType, Object data, String date) {
        this.notifyType = notifyType;
        this.data = data;
        this.date = date;
    }



    public NotifyData() {
    }

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
