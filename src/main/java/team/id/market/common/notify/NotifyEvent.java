package team.id.market.common.notify;

import org.springframework.context.ApplicationEvent;

import java.util.Date;

/**
 * @author 431587
 * @date 2023/6/13 16:18
 */
public class NotifyEvent extends ApplicationEvent {

    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public NotifyEvent(Object source, String notifyType, Object data, String date) {
        super(source);
        this.notifyType = notifyType;
        this.data = data;
        this.date = date;
    }


    private String notifyType;

    private Object data;

    private String date;

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
