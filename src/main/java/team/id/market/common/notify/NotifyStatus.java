package team.id.market.common.notify;

/**
 * @author 431587
 * @date 2023/6/13 15:22
 */
public class NotifyStatus {

    /*车辆在线*/
    public static final String VEHICLE_INLINE = "VEHICLE_INLINE";

    /*车辆上线线*/
    public static final String VEHICLE_ONLINE = "VEHICLE_ONLINE";

    /*RMI连接断开*/
    public static final String RMI_DISCONNECT = "RMI_DISCONNECT";

    /*车辆离线*/
    public static final String VEHICLE_LOSE_CONNECT = "VEHICLE_LOSE_CONNECT";
    /*车辆端口异常*/
    public static final String VEHICLE_PORT_ERROR = "VEHICLE_PORT_ERROR";
    /*超过重试次数*/
    public static final String VEHICLE_OVER_RETRY_COUNT = "VEHICLE_OVER_RETRY_COUNT";

    public static final String VEHICLE_OVER_RETRY_RESET = "VEHICLE_OVER_RETRY_RESET";




}
