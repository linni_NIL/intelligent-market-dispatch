package team.id.market.common.pool;

import org.springframework.stereotype.Component;
import team.id.market.navigation.entity.MapPathInfo;
import team.id.market.navigation.entity.MapPointInfo;
import team.id.market.navigation.entity.simulation.MapPointInfoTO;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author wsh
 * @date 2023/10/31 01:56
 */
@Component("ObjectPool")
public class ObjectPool {

    private static Map<String, List<MapPointInfoTO>> pointInfoMap = new ConcurrentHashMap<>();

    private static Map<String, List<MapPathInfo>> pathInfoMap = new ConcurrentHashMap<>();

    private static Map<String,String> floorInfoMap = new ConcurrentHashMap<>();


    public static Map<String, List<MapPointInfoTO>> getPointInfoMap() {
        return pointInfoMap;
    }

    public static Map<String, List<MapPathInfo>> getPathInfoMap() {
        return pathInfoMap;
    }

    public static Map<String, String> getFloorInfoMap() {
        return floorInfoMap;
    }
}
