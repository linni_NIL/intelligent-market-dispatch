package team.id.market.common.result;



import team.id.market.common.enums.CodeEnum;
import team.id.market.common.enums.CodeStatus;
import team.id.market.common.exception.MessageException;
import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

/**
 * 通用返回对象,分为两种，一种成功一种失败，同时状态可在codeEnum枚举中查询
 * Created by achims on 2019/9/25.
 */
@ToString
public class CommonResult<T> {

    @ApiModelProperty(value = "状态码")
    private long status;
    @ApiModelProperty(value = "状态码对应的含义")
    private String msg;
    @ApiModelProperty(value = "返回值")
    private T result;

    public CommonResult() {
    }

    protected CommonResult(long status, String message, T result) {
        this.status = status;
        this.msg = message;
        this.result = result;
    }

    public static <T> CommonResult<T> success() {
        return new CommonResult<T>(CodeEnum.SUCCESS.status(), CodeEnum.SUCCESS.msg(), null);
    }

    public static <T> CommonResult<T> success(T data) {
        return new CommonResult<T>(CodeEnum.SUCCESS.status(), CodeEnum.SUCCESS.msg(), data);
    }

    public static <T> CommonResult<T> success(T data, String message) {
        return new CommonResult<T>(CodeEnum.SUCCESS.status(), message, data);
    }


    public static <T> CommonResult<T> failed(CodeStatus codeEnum) {
        return new CommonResult<T>(codeEnum.status(), codeEnum.msg(), null);
    }

    public static <T> CommonResult<T> failed(CodeStatus codeEnum, T data) {
        return new CommonResult<T>(codeEnum.status(), codeEnum.msg(), data);
    }


    public static <T> CommonResult<T> failed(String message) {
        return new CommonResult<T>(CodeEnum.FAILED.status(), message, null);
    }

    public static <T> CommonResult<T> failed(String message, T data) {
        return new CommonResult<T>(CodeEnum.FAILED.status(), message, data);
    }

    public static <T> CommonResult<T> failed(CodeEnum codeEnum, T data) {
        return new CommonResult<T>(codeEnum.getStatus(), codeEnum.getMsg(), data);
    }

    public static <T> CommonResult<T> failed(Integer status, String message) {
        return new CommonResult<T>(status, message, null);
    }

    public static <T> CommonResult<T> failed(MessageException message) {

        return new CommonResult<T>(message.getStatus(), message.getMessage(), (T) message.getErrorMessage());
    }

    public static <T> CommonResult<T> failed() {
        return failed(CodeEnum.FAILED);
    }


    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}
