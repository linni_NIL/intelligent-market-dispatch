package team.id.market.common.result;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

/**
 * Desc : 作为自定义分页实体，用于分页显示
 * Created by achims on 2019/9/25.
 */
@Data
@ApiModel(value = "分页返回数据")
public class PageBean<T> {

    /**
     * 当前页
     */
    @ApiModelProperty(value = "当前页")
    private Integer currentPage;
    /**
     * 每页显示的总条数
     */
    @ApiModelProperty(value = "每页显示的总条数")
    private Integer pageSize;
    /**
     * 总条数
     */
    @ApiModelProperty(value = "总条数")
    private long totalNum;
    /**
     * 是否有下一页
     */
    @ApiModelProperty(value = "是否有下一页")
    private boolean isMore;
    /**
     * 总页数
     */
    @ApiModelProperty(value = "总页数")
    private long totalPage;
    /**
     * 开始索引
     */
    @ApiModelProperty(value = "开始索引")
    private Integer startIndex;
    /**
     * 分页结果
     */
    @ApiModelProperty(value = "分页结果")
    private List<T> item;

    public PageBean() {

    }

    public PageBean(Integer currentPage, Integer pageSize, long totalNum) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalNum = totalNum;
        this.totalPage = (this.totalNum + this.pageSize - 1) / this.pageSize;
        this.startIndex = (this.currentPage - 1) * this.pageSize;
        this.isMore = this.currentPage < this.totalPage;
    }

    public PageBean(Integer currentPage, Integer pageSize, long totalNum, List<T> item) {
        this.currentPage = currentPage;
        this.pageSize = pageSize;
        this.totalNum = totalNum;
        this.totalPage = (this.totalNum + this.pageSize - 1) / this.pageSize;
        this.startIndex = (this.currentPage - 1) * this.pageSize;
        this.isMore = this.currentPage < this.totalPage;
        this.item = item;
    }


    /**
     * 将PageHelper分页后的list转为分页信息
     */
    public static <T> PageBean<T> restPage(List<T> list) {
        PageBean<T> result = new PageBean<T>();
        PageInfo<T> pageInfo = new PageInfo<T>(list);
        result.setTotalPage(pageInfo.getPages());
        result.setCurrentPage(pageInfo.getPageNum());
        result.setPageSize(pageInfo.getPageSize());
        result.setTotalNum(pageInfo.getTotal());
        result.setItem(new ArrayList<>(pageInfo.getList()));
        return result;
    }

    /**
     * 将SpringData分页后的list转为分页信息
     */
    public static <T> PageBean<T> restPage(Page<T> pageInfo) {
        PageBean<T> result = new PageBean<T>();
        result.setTotalPage(pageInfo.getTotalPages());
        result.setCurrentPage(pageInfo.getNumber());
        result.setPageSize(pageInfo.getSize());
        result.setTotalNum(pageInfo.getTotalElements());
        result.setItem(new ArrayList<>(pageInfo.getContent()));
        return result;
    }

}
