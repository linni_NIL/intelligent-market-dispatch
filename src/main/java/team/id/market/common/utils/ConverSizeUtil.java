package team.id.market.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * @author 260654
 * @date 2023/4/28 17:24
 */
public  class ConverSizeUtil {
    /**
     * 将文件大小由Byte转为MB或者KB
     * @return
     */
    public static String fileSizeByteToM(Long size) {

        BigDecimal fileSize = new BigDecimal(size);
        BigDecimal param = new BigDecimal(1024);
        int count = 0;
        while (fileSize.compareTo(param) > 0 && count < 5) {
            fileSize = fileSize.divide(param);
            count++;
        }
        DecimalFormat df = new DecimalFormat("#.##");
        String format = df.format(fileSize) + "";
        String[] split = format.split("\\.");
        String result = split[0];
        switch (count) {
            case 0:
                result += "B";
                break;
            case 1:
                result += "KB";
                break;
            case 2:
                result += "MB";
                break;
            case 3:
                result += "GB";
                break;
            case 4:
                result += "TB";
                break;
            case 5:
                result += "PB";
                break;
        }
        return result;
    }
}
