package team.id.market.common.utils;


import team.id.market.common.enums.CodeEnum;
import team.id.market.common.exception.MessageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Desc : 日期转换类
 * Created by achims on 2020/1/9.
 */
public class DateUtil {

    public static final String YEAR = "year";
    public static final String MONTH = "month";
    public static final String DAY = "day";
    public static final String HOUR = "hour";


    static Logger logger = LoggerFactory.getLogger(DateUtil.class);

    /**
     * 将日期转成字符串
     *
     * @param date    日期
     * @param pattern 日期格式
     * @return
     */
    public static String date2String(Date date, String... pattern) {
        try {
            SimpleDateFormat sdf;
            if (pattern == null || pattern.length == 0) {
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            } else {
                sdf = new SimpleDateFormat(pattern[0]);
            }
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String date2String(String date, String... pattern) {
        Date date1 = strToDate(date, pattern);
        return date2String(date1, pattern);
    }


    /**
     * 日期字符串转换成日期
     *
     * @param time
     * @param pattern
     * @return
     */

    public static Date strToDate(String time, String... pattern) {
        try {
            SimpleDateFormat sdf;
            if (pattern == null || pattern.length == 0) {
                sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            } else {
                sdf = new SimpleDateFormat(pattern[0]);
            }
            return sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
            throw new MessageException("字符串转日期存在问题");//日期错误
        }
    }

    /**
     * 获取指定月最后天
     *
     * @param date
     * @return
     */
    public static Integer getCurrentLastMouthOfDay(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        return c.get(Calendar.DAY_OF_MONTH);
    }

    //todo 后期会修改成一个全局可用的

    /**
     * 在现有基础上加时间(后期会修改)
     *
     * @param date 需要修改的时间
     * @param time
     * @return
     */
    public static Date addHour(Date date, Integer time) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.HOUR, time);
        return c.getTime();
    }

    public static long betweenMin(Date begin, Date end) {
//        LocalDate beginDate = begin.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//        LocalDate endDate = end.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
//
//        Duration between = Duration.between(endDate, beginDate);
//        return between.toMinutes();
//        //todo 复用性不高，后期改
        long between = end.getTime() - begin.getTime();
        long day = between / (24 * 60 * 60 * 1000);
        long hour = (between / (60 * 60 * 1000) - day * 24);
        long min = ((between / (60 * 1000)) - day * 24 * 60 - hour * 60);
        return min;
    }

    /**
     * 获取年月日的数据
     *
     * @param date
     * @return
     */
    public static int getTimeWithType(Date date, String type) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int value = -1;
        switch (type) {
            case YEAR:
                value = calendar.get(Calendar.YEAR);
                break;
            case MONTH:
                value = calendar.get(Calendar.MONTH) + 1;
                break;
            case DAY:
                value = calendar.get(Calendar.DAY_OF_MONTH) + 1;
        }
        return value;
    }


    /**
     * 获取这段时间段的所有日期
     *
     * @param startDay 开始时间
     * @param endDay   结束时间
     * @param pattern  转换的类型
     * @return
     */
    public static List<String> getPeriod(Date startDay, Date endDay, Boolean containEndDay, String... pattern) {
        List<String> returnList = new ArrayList<>();
        SimpleDateFormat sdf;
        try {
            if (pattern == null || pattern.length == 0) {
                sdf = new SimpleDateFormat("yyyy-MM-dd");
            } else {
                sdf = new SimpleDateFormat(pattern[0]);
            }
            Calendar tempStart = Calendar.getInstance();
            tempStart.setTime(startDay);
            Calendar tempEnd = Calendar.getInstance();
            tempEnd.setTime(endDay);
            while (tempStart.before(tempEnd)) {
                returnList.add(sdf.format(tempStart.getTime()));
                tempStart.add(Calendar.DAY_OF_YEAR, 1);
            }
            if (containEndDay == true) {
                returnList.add(sdf.format(tempStart.getTime()));
                tempStart.add(Calendar.DAY_OF_YEAR, 1);
            }

        } catch (Exception e) {
            throw new MessageException("转换时间失败");
        }
        return returnList;

    }

    /**
     * @param curt 当前时间
     * @param time 几个月
     * @return
     */
    public static Date afterTimeWithType(Date curt, String type, Integer time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(curt);
        switch (type) {
            case YEAR:
                calendar.add(Calendar.MONTH, time);
                break;
            case MONTH:
                calendar.add(Calendar.MONTH, time);
                break;
            case DAY:
                calendar.add(Calendar.MONTH, time);
                break;
            case HOUR:
                calendar.add(Calendar.HOUR, time);
        }
        return calendar.getTime();
    }

    public static Date beforeTimeWithType(Date curt, String type, int month) {
        return afterTimeWithType(curt, type, -month);
    }


    /**
     * 秒转换成天:时:分:秒显示
     *
     * @param time
     * @return
     */
    public static String sec2time(Long time) {
        //单位
        Integer mi = 60;//60秒
        Integer hh = mi * 60;//60分
        Integer dd = hh * 24;//24小时
        Long day = time / dd;
        Long hour = (time - day * dd) / hh;
        Long minute = (time - day * dd - hour * hh) / mi;
        Long secoud = (time - day * dd - hour * hh - minute * mi);

        if (day > 0) {
            return day + "d" + hour + "h" + minute + "m" + secoud + "s";
        } else if (hour > 0) {
            return hour + "h" + minute + "m" + secoud + "s";
        } else if (minute > 0) {
            return minute + "m" + secoud + "s";
        } else if (secoud > 0) {
            return secoud + "s";
        } else {
            return "0s";
        }
    }

    /**
     * 检测发送的时间是否存在不合理情况
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @param throwExc  是否抛出异常
     * @param parttern  样式
     * @return
     */
    public static boolean checkTime(String startTime, String endTime, Boolean throwExc, String... parttern) {
        SimpleDateFormat simpleDateFormat = null;
        try {
            if (parttern != null && parttern.length > 0) {
                simpleDateFormat = new SimpleDateFormat(parttern[0]);
            } else {
                simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            }
            Date startDate = simpleDateFormat.parse(startTime);
            Date endDate = simpleDateFormat.parse(endTime);
            if (startDate.getTime() > endDate.getTime()) {
                if (throwExc == true)
                    throw new MessageException(CodeEnum.GL10003);
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }
}
