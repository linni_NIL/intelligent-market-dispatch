package team.id.market.common.utils;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 * @author 260655
 */
public class DruidPoolUtils {
    static Logger logger = LoggerFactory.getLogger(DruidPoolUtils.class);
    private DruidPoolUtils(){
        throw new IllegalStateException("Utility classes");
    }

    /**
     * 创建成员变量datasource
     */
    private static DataSource dataSource;
    //加载配置文件
    static {
        try {
            Properties properties = new Properties();
            //加载类路径
            properties.load(DruidPoolUtils.class.getResourceAsStream("/druid.properties"));

            //读取属性文件，创建连接池
            dataSource = DruidDataSourceFactory.createDataSource(properties);
        }catch (Exception e){
            logger.error("druid配置文件读取失败",e);
        }
    }

    /**
     * 获取连接对象
     * @return 获取连接对象
     */
    public static Connection getConnection(){
        try {
            return dataSource.getConnection();
        }catch (SQLException e){
            logger.warn("连接池对象获取错误");
            return null;
        }
    }

    //释放资源

    public static void close(Connection connection, Statement statement, ResultSet resultSet){
        if (resultSet != null ){
            try {
                resultSet.close();
            }catch (SQLException e){
                logger.error("resultSet关闭失败",e);
            }
        }
        if (statement != null){
            try {
                statement.close();
            }catch (SQLException e){
                logger.error("statement关闭失败",e);
            }
        }
        if (connection != null){
            try {
                connection.close();
            }catch (SQLException e){
                logger.error("connection关闭失败",e);
            }
        }
    }

    public static void close(Connection connection,Statement statement){
        close(connection,statement,null);
    }
}
