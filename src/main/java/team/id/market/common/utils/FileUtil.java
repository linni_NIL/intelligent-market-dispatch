package team.id.market.common.utils;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * @author 431587
 * @date 2023/5/23 16:19
 */
public class FileUtil {
    private static Logger logger = LoggerFactory.getLogger(FileUtil.class);
    /**
     * 构建目录
     *
     * @param outputDir 输出目录
     * @param subDir    子目录
     */
    public static File createDirectory(String outputDir, String subDir) {
        File file = new File(outputDir);
        // 子目录不为空
        if (!(subDir == null || "".equals(subDir.trim()))) {
            file = new File(outputDir + File.separator + subDir);

        }
        if (!file.exists()) {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            file.mkdirs();
            return file;
        } else {
            logger.warn("目录创建失败，目录{}不为空", outputDir + subDir);
            return null;
        }
    }
    //获取流文件
    public static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 判断解压的文件在解压目录下是否存在
     *
     * @param targetPath
     * @param FileName
     * @return
     */
    public static boolean checkFileExistence(String targetPath, String FileName) {


        boolean exist = false;
        File target = new File(targetPath);
        if (target.isDirectory()) {
            File[] files = target.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    String fileName = file.getName();
                    if (fileName.equals(FileName)) {
                        exist = true;
                        break;
                    }
                }
            }
        }
        return exist;
    }

    /**
     * 删除文件夹及其下的所有子文件夹和文件
     *
     * @param folderPath
     */
    public static void deleteFolder(String folderPath) {
        try {
            File baseFolder = new File(folderPath);
            if (baseFolder.exists()) {
                deleteAllFileInFolder(folderPath);
                baseFolder.delete();
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public static boolean deleteAllFileInFolder(String path) {
        boolean flag = false;
        File file = new File(path);
        if (!file.exists()) {
            return flag;
        }
        if (!file.isDirectory()) {
            return flag;
        }
        String[] tempList = file.list();
        File temp = null;
        for (int i = 0; i < tempList.length; i++) {

            if (path.endsWith(File.separator)) {
                temp = new File(path + tempList[i]);
            } else {
                temp = new File(path + File.separator + tempList[i]);
            }
            if(temp.isFile()){
                temp.delete();
            }
            if (temp.isDirectory()) {
                deleteAllFileInFolder(path + File.separator + tempList[i]);
                deleteFolder(path + File.separator + tempList[i]);
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 删除指定文件
     *
     * @param path
     */
    public static void deleteFile(final String path) {
        final File file = new File(path);
        if (file.exists() && file.isFile()) {
            file.delete();
        }
    }

    public static String getImgBase(String imgFile) {

        // 将图片文件转化为二进制流
        InputStream in = null;
        byte[] data = null;
        // 读取图片字节数组
        try {
            in = new FileInputStream(imgFile);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 图片头
        //String imghead = "data:image/jpeg;base64,";
        return Base64.encodeBase64String(data);
    }
}