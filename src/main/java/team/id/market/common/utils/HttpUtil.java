package team.id.market.common.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Map;

/**
 * http方面的工具类
 * <p>
 * Created by achims on 2020/3/30.
 */
public class HttpUtil {
    public static final String XLS = "xls";
    public static final String XLSX = "xlsx";
    private static String[] IEBrowserSignals = {"MSIE", "Trident", "Edge"};

    public static final String RESPONSE_SUCCESS = "success";
    public static final String RESPONSE_FAILED = "failed";

    private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

    public static boolean isMSBrowser(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        for (String signal : IEBrowserSignals) {
            if(userAgent.contains(signal)){
                return true;
            }
        }
        return false;
    }


    /**
     * 导出 excel 文档
     *
     * @param response
     * @param wb
     */
    public static void doExport(HttpServletResponse response,
                                Workbook wb) {
        try (OutputStream os = response.getOutputStream()) {
            response.setContentType("application/vnd.ms-excel");
            String name ="路网设计文件"+ DateUtil.date2String(new Date(), "yyyy-MM-dd")+"[文件不可更改，请谨慎操作！]";
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            boolean isMSIE = isMSBrowser(request);
            if (isMSIE) {
                name = URLEncoder.encode(name, "UTF-8");
            } else {
                name = new String(name.getBytes("UTF-8"), "ISO-8859-1");
            }
            name = "[文件不可更改，请谨慎操作！]";
            response.setHeader("Content-disposition", "attachment;filename=" + name + ".xlsx");
            response.setContentType("application/octet-stream");
            response.setCharacterEncoding("UTF-8");
            wb.write(os);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 导出 excel 文档
     *
     * @param request
     * @param response
     * @param wb       工作普
     * @param name     名字
     * @param pattern  格式类型
     */
    public static void doExport(HttpServletRequest request, HttpServletResponse response,
                                Workbook wb, String name, String pattern) {
        try (OutputStream os = response.getOutputStream()) {
            response.setContentType("application/octet-stream");
            boolean isMSIE = isMSBrowser(request);
            if (isMSIE) {
                name = URLEncoder.encode(name, "UTF-8");
            } else {
                name = new String(name.getBytes("UTF-8"), "ISO-8859-1");
            }
            response.setCharacterEncoding("UTF-8");
            if (pattern.equals(XLS)) {
                response.setContentType("application/vnd.ms-excel");//03版本
            } else {
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");//07版本
            }
            response.setHeader("Content-disposition", "attachment;filename=" + name + "." + pattern);
            response.setHeader("Access-Control-Expose-Headers", "Content-disposition");//使得跨域资源得到filename 不设置没法得到
            HttpUtil.res(request, response);//跨域资源共享
            wb.write(os);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * mime的设置以及响应头
     *
     * @param response
     * @param file     文件
     * @param name     想要输出的文件名
     */
    public static void doUpload(HttpServletRequest request, HttpServletResponse response, File file, String name) throws UnsupportedEncodingException {
        name = name + file.getName().substring(file.getName().lastIndexOf("."));//得到后缀
        response.setContentType("application/octet-stream");
        boolean isMSIE = isMSBrowser(request);
        if (isMSIE) {
            name = URLEncoder.encode(name, "UTF-8");
        } else {
            name = new String(name.getBytes("UTF-8"), "ISO-8859-1");
        }
        //跨域资源问题
        HttpUtil.res(request, response);//跨域资源共享
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/octet-stream;charset=utf-8");//流发方式下载文件
        response.setHeader("Content-disposition", "attachment;filename=" + (name == null ? file.getName() : name));
        response.setHeader("Access-Control-Expose-Headers", "Content-disposition");//使得跨域资源得到filename 不设置没法得到
        FileInputStream input = null;
        OutputStream outputStream = null;
        try {
            input = new FileInputStream(file);
            outputStream = response.getOutputStream();
            byte[] buffer = new byte[1028];
            int r = 0;
            while ((r = input.read(buffer)) != -1) {
                outputStream.write(buffer, 0, r);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
    public static void res(HttpServletRequest request, HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("origin"));
        response.setHeader("Access-Control-Allow-Methods", "DELETE,OPTIONS,GET,POST,PUT");
        response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=utf-8");
    }

    /**
     * 返回信息
     *
     * @param response
     * @param request
     * @param obj      数据
     */
    public static void res(HttpServletResponse response, HttpServletRequest request, Object obj) {
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("origin"));
        response.setHeader("Access-Control-Allow-Methods", "DELETE,OPTIONS,GET,POST,PUT");
        response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=utf-8");
        try (PrintWriter out = response.getWriter()) {
            out.write(String.valueOf(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 发送 post 请求
     *
     * @param url    接口地址
     * @param params 请求参数
     * @param header 请求头
     * @return
     */
    public static String doPost(String url, Map<String, String> header, String params) {
        if (StringUtils.isEmpty(url)) {
            return "url cannot not be null";
        }
        HttpPost httpPost = null;

        try {
            HttpClient httpClient = HttpClients.createDefault();
            httpPost = new HttpPost(url);

            //设置请求头
            if (header != null && header.size() > 0) {
                for (String key : header.keySet()) {
                    httpPost.setHeader(key, header.get(key));
                }
            }
            //设置消息实体
            if (params != null) {
                StringEntity entity = new StringEntity(params, Charset.forName("UTF-8"));
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json");
                httpPost.setEntity(entity);
            }

            //设置超时
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectionRequestTimeout(50000)
                    .setConnectTimeout(50000)
                    .setSocketTimeout(50000).build();

            httpPost.setConfig(requestConfig);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity httpEntity = httpResponse.getEntity();
                return EntityUtils.toString(httpEntity);
            } else {
                return "";
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
    }




    public static String enableCharge(String isEnable) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:55200/v1/enableAutoCharge?isEnable=" + isEnable);
        CloseableHttpResponse response = httpClient.execute(httpGet);
        String responseMsg = "";
        if (response != null && response.getStatusLine().getStatusCode() == 200){
            logger.info("请求修改开启充电接口成功，响应信息为{}",response);
            responseMsg =  RESPONSE_SUCCESS;
        }else{
            logger.info("请求修改开启充电接口失败，响应信息为{}",response);
            responseMsg = RESPONSE_FAILED;
        }
        if (response != null){
            response.close();
        }
        return responseMsg;

    }


    public static String updateKernelIntegrationLevel(String vehicleName, String integrationLevel) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut("http://localhost:55200/v1/vehicles/"
                +vehicleName + "/integrationLevel?newValue=" + integrationLevel);
        CloseableHttpResponse response = httpClient.execute(httpPut);
        String responseMsg = "";
        if (response != null && response.getStatusLine().getStatusCode() == 200){
            logger.info("请求修改集成级别接口成功，响应信息为{}",response);
            responseMsg =  RESPONSE_SUCCESS;
        }else{
            logger.info("请求修改集成级别接口失败，响应信息为{}",response);
            responseMsg = RESPONSE_FAILED;
        }
        if (response != null){
            response.close();
        }
        return responseMsg;

    }

}
