package team.id.market.common.utils;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.github.pagehelper.util.StringUtil;
import team.id.market.common.enums.CodeEnum;
import team.id.market.common.excel.ExcelEnum;
import team.id.market.common.excel.ImportAndExport;
import team.id.market.common.exception.MessageException;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by achims on 2019/9/26.
 * 该类是excel导入导出类 ps 需要与ImportAndExport注解一同使用
 * 目的是为了减少一些非必要代码的编写，如手动编写代码读取导出excel，同时能快速查找错误下标以及重复数据下标（需要重写eqauls）
 * 1、只需要调用exportPageExcel接口：代表导出类
 * 2、以及readExcelValue接口：代表导入类，需要根据要求重写equal方法，如根据该行哪些字段要求唯一
 */
@Component
public class ImportAndExportUtil {
    public static final String WRONG_SHEET = "文件内容和模板不符，请检查后重新导入";
    public static final String EMPTY_CELL = "部分必填字段为空，请检查后重新导入";
    public static final String WRONG_FORMAT = "文件存在错误格式内容，请检查后重新导入";
    public static final String REPEAT_VALUE = "文件内容有重复，请检查后重新导入";
    public static final String LONG_LENGTH_VALUE = "数据长度过长，最大长度为";
    public static final String SHORT_LENGTH_VALUE = "数据长度过短，最小长度为";
    public static final String MESSAGE="请检查后重新导入";
    public static final Integer DEFAULT_CELL_WIDTH = 2018;
    public static final String PATTERN = "(.xls|.xlsx)$";
    public static final  String PHONE_ERROR="手机号码格式错误";


    /**
     * 导出excel
     *
     * @return
     */
    public <T> void exportPageExcel(Workbook wb, String sheetName, List<T> entities, Class<T> clazz) {
        Sheet sheet = wb.createSheet(sheetName);
        int curtRow = 0;
        if (entities == null || entities.size() == 0){
            return;
        }
        Set<String> title = doGetTitle(clazz);//文件头
        createColumnNames(wb, sheet, curtRow++, new ArrayList<String>(title));
        for (T entity : entities) {
            Row contentRow = sheet.createRow(curtRow++);
            doSetContentVal(contentRow, entity, clazz);//根据
        }
    }

    /**
     * 导出excel2
     * 第一行合并
     *
     * @return
     */
    public <T> void exportPageExcel2(Workbook wb, List<T> entities, Class<T> clazz,String firstRowName) {
        Sheet sheet = wb.createSheet();
        int curtRow = 1;
        if (entities == null || entities.size() == 0){
            return;
        }

        Set<String> title = doGetTitle(clazz);//文件头
        Row row = sheet.createRow(0);
        CellStyle cellStyle=wb.createCellStyle();
        DataFormat dataFormat =wb.createDataFormat();
        cellStyle.setDataFormat(dataFormat.getFormat("@"));
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        sheet.addMergedRegion(new CellRangeAddress(0,0,0,title.size()-1));
        Cell cell = row.createCell(0);
        cell.setCellValue(firstRowName);
        cell.setCellStyle(cellStyle);
        createColumnNames(wb, sheet, curtRow++, new ArrayList<String>(title));
        for (T entity : entities) {
            Row contentRow = sheet.createRow(curtRow++);
            doSetContentVal2(cellStyle,contentRow, entity, clazz);//根据
        }
    }

    public  <T> Set<String> doGetTitle(Class<T> clazz) {
        Set<String> resultSet = new LinkedHashSet<>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            ImportAndExport export = field.getAnnotation(ImportAndExport.class);
            if (export == null){
                continue;
            }
            resultSet.add(export.desc());
        }
        return resultSet;
    }

    /**
     * 创建列头
     *
     * @param wb
     * @param sheet
     * @param startRow 开始行
     * @param title    列头
     */
    public void createColumnNames(Workbook wb, Sheet sheet, int startRow, ArrayList<String> title) {
//        HSSFWorkbook workbook=(HSSFWorkbook) wb;
        Row titleRow = sheet.createRow(startRow);
        CellStyle cellStyle = wb.createCellStyle();
        DataFormat dataFormat =wb.createDataFormat();
        int i = 0;
        for (String key : title) {
            int length = title.get(i).getBytes().length * 256;
            sheet.setColumnWidth(i, length > DEFAULT_CELL_WIDTH ? DEFAULT_CELL_WIDTH * 2 : DEFAULT_CELL_WIDTH);
            cellStyle.setWrapText(true);
            cellStyle.setDataFormat(dataFormat.getFormat("@"));
            cellStyle.setAlignment(HorizontalAlignment.CENTER);//
            Cell cell = titleRow.createCell(i);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(key);
            sheet.setDefaultColumnStyle(i,cellStyle);
            i++;
        }
    }

    /**
     * 填充一行数据
     *
     * @param contentRow 当前行
     * @param entity     实体
     * @param clazz      实体类型
     * @param <T>
     */
    private   <T> void doSetContentVal(Row contentRow, T entity, Class<T> clazz) {
        List<String> content = doGetNeedExportField(entity, clazz);
        for (int i = 0; i < content.size(); i++) {
            Cell cell = contentRow.createCell(i);
            cell.setCellValue(content.get(i));
        }
    }
    private   <T> void doSetContentVal2(CellStyle cellStyle,Row contentRow, T entity, Class<T> clazz) {
        List<String> content = doGetNeedExportField(entity, clazz);
        for (int i = 0; i < content.size(); i++) {
            Cell cell = contentRow.createCell(i);
            cell.setCellStyle(cellStyle);
            cell.setCellValue(content.get(i));
        }
    }

    /**
     * 获取实体需要导出的字段
     * 可能科学记数法
     *
     * @param entity
     * @param clazz  实体类型
     * @param <T>
     * @return desc, val
     */
    public  <T> List<String> doGetNeedExportField(T entity, Class<T> clazz) {
        List<String> resultList = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                ImportAndExport export = field.getAnnotation(ImportAndExport.class);
                if (export == null){
                    continue;
                }
                Object o = field.get(entity);
                String val = null;
                if (o == null) {
                    resultList.add("");
                    continue;
                }

                if (o instanceof Double) {
                    val = new BigDecimal(o.toString()).toPlainString();
                } else if (o instanceof Date || o instanceof LocalDate || o instanceof LocalDateTime || o instanceof LocalTime) {
                    val = timeTransferWithLocalDate(o, export.format());
//                    val = !StringUtil.isEmpty(export.format()) ?
//                            DateUtil.format((Date) o, export.format()) :
//                            DateUtil.format((Date) o, "yyyy-MM-dd");
                } else {
                    //需要进行转换
                    if (export.valueTransfer() != null && export.valueTransfer().length > 0) {
                        String[] strings = export.valueTransfer();
                        //获取数据转换的类型
                        Map<String, String> key = new HashMap<>();
                        Arrays.asList(strings).stream().forEach(e -> {
                            String[] s = e.split("_");//
                            key.put(s[0], s[1]);
                        });
                        val = key.get(o.toString().trim());
                    } else {
                        val = o.toString();//数据全设置为字符串类型，同时数据为空的话为 ""
                    }
                }
                resultList.add(val);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }

    public String timeTransferWithLocalDate(Object currentValue, String format) {
        if (currentValue instanceof Date) {
            return !StringUtil.isEmpty(format) ?
                    DateUtil.format((Date) currentValue, format) :
                    DateUtil.format((Date) currentValue, "yyyy-MM-dd");
        }
        if (format == null) {
            return currentValue.toString();
        }

        if (currentValue instanceof LocalTime) {
            return ((LocalTime) currentValue).format(DateTimeFormatter.ofPattern(format));
        } else if (currentValue instanceof LocalDateTime) {
            return ((LocalDateTime) currentValue).format(DateTimeFormatter.ofPattern(format));
        } else if (currentValue instanceof LocalDate) {
            return ((LocalDate) currentValue).format(DateTimeFormatter.ofPattern(format));
        }
        return currentValue.toString();
    }


    /****************************************当前以下为导入部分******************************************************************************************/

    public static final String TYPE = ".+(.xls|.xlsx)$";

    /**
     * 导入文件
     *
     * @param file  某文件
     * @param clazz 某种class对象
     * @param <T>
     * @return 导入的集合
     */
    public <T> List<T> readExcelValue(MultipartFile file, Class<T> clazz,String sheetName) {

        String fileName = file.getOriginalFilename();
        Pattern pattern = Pattern.compile(TYPE);
        Matcher matcher = pattern.matcher(fileName);
        if (!matcher.find()) {
            throw new MessageException(CodeEnum.GL31010);
        }
        Workbook workbook = null;
        List<T> ts = new ArrayList<>();
        try {
            workbook = WorkbookFactory.create(file.getInputStream());
            ts = readExcelValue(workbook, clazz,sheetName);
        } catch (MessageException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new MessageException("文件读取错误，请确认当前文件是否已损坏");
        }
        return ts;
    }

    /**
     * 使用该方法需要注意的是重写equals以及hascode，以判断上传数据是否存在重复的情况
     *
     * @param wb
     * @param clazz 某种class对象
     * @param <T>   后期返回数据根据当前标识来返回，就不需要向下造型，省的很多麻烦
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    public <T> List<T> readExcelValue(Workbook wb, Class<T> clazz,String targetSheetName) throws MessageException, InstantiationException, IllegalAccessException {
        Set<T> beans = new LinkedHashSet<>();
        int numberOfSheets = wb.getNumberOfSheets();
        for (int sheetIndex = 0; sheetIndex < numberOfSheets; sheetIndex++) {//从sheet页开始
            Sheet sheetAt = wb.getSheetAt(sheetIndex);
            String sheetName = sheetAt.getSheetName();
            if(sheetName.equals(targetSheetName)){
                int firstRowNum = sheetAt.getFirstRowNum();//读取的起点
                if("com.leayun.cn.publicAuth.dto.ExcelDTO".equals(clazz.getName())){
                    firstRowNum++;
                }
                int lasRow = sheetAt.getLastRowNum();//读取的终点
                Row title = sheetAt.getRow(firstRowNum);//读取文件头
                if (title != null && !PoiUtils.isRowEmpty(title)) {
                    Field[] declaredFields = getFieldWithExportAnnotion(clazz);//获得拥有export注解的字段
                    Map<String, Integer> headIndex = getHeadIndex(declaredFields, title);
                    if(declaredFields.length!=title.getLastCellNum()){
                        throw new MessageException(CodeEnum.GL20008.getStatus(),WRONG_SHEET);
                    }
                    if (headIndex == null) {
                        continue;//当前页没有数据
                    } else if (headIndex.size() != declaredFields.length) {
                        throw new MessageException(CodeEnum.GL20008.getStatus(), WRONG_SHEET);
                    }
                    for (int rowIndex = firstRowNum + 1; rowIndex <= lasRow; rowIndex++) {//读取每行的数据，每一行的数据
                        Row value = sheetAt.getRow(rowIndex);
                        Integer wrongY = rowIndex;//这里表示的是错误信息 多少行
                        if (value == null || PoiUtils.isRowEmpty(value))//假如当前行的为空行 或者没有数据的话，跳过当前行
                        {
                            continue;
                        }
                        Integer wrongX = null;
                        T obj = clazz.newInstance();//反射来得到对象
                        for (Field field : declaredFields) {
                            wrongX = headIndex.get(field.toString());
                            if (wrongX != null) {
                                Cell cellValue = value.getCell(headIndex.get(field.toString()));//通过注解找到对应的下标
                                String stringValueFromCell = (PoiUtils.getStringValueFromCell(cellValue) == null ? "" : PoiUtils.getStringValueFromCell(cellValue));
                                stringValueFromCell = stringValueFromCell.replace((char) 12288, ' ').trim();//将中文空格修改为英文空格
                                ImportAndExport export = field.getAnnotation(ImportAndExport.class);//切换为 export中的一个方法代替IsNeed注解
                                if (export != null) {
                                    //数据为空
                                    if (export.isNeed() == true && (stringValueFromCell == null || stringValueFromCell.trim().equals(""))) {
                                        throw new MessageException(CodeEnum.GL20008.getStatus(), EMPTY_CELL );//错误地址
                                    }
                                    //正则出现问题
                                    if (!StringUtils.isEmpty(export.pattern()) && !pattern(export.pattern(), stringValueFromCell, export.isNeed())) {
                                        throw new MessageException(CodeEnum.GL20009.getStatus(), export.patternMessage());//错误地址
                                    }

                                    //发送限制的数组数据
                                    if (export.limitValue().length != 0) {
                                        if (!Arrays.asList(export.limitValue()).contains(stringValueFromCell)) {
                                            throw new MessageException(CodeEnum.GL20010.getStatus(), WRONG_FORMAT );//错误地址
                                        }
                                    }
                                    //长度限制
                                    if (stringValueFromCell.length() > export.max()) {
                                        throw new MessageException(CodeEnum.GL20009.getStatus(), export.desc()+LONG_LENGTH_VALUE + export.max() + "," + MESSAGE);
                                    }

                                    if (stringValueFromCell.length() < export.min()) {
                                        throw new MessageException(CodeEnum.GL20009.getStatus(), export.desc()+SHORT_LENGTH_VALUE + export.min() + "," + MESSAGE);
                                    }

                                }
                                String methodName = MethodUtils.setMethodName(field.getName());//得到方法
                                Method method = null;
                                try {
                                    method = clazz.getMethod(methodName, field.getType());
                                    method.invoke(obj, convertType(field.getType(), stringValueFromCell.trim()));//将数据装载
                                } catch (NoSuchMethodException | InvocationTargetException e) {
                                    e.printStackTrace();
                                    throw new MessageException(CodeEnum.FAILED, WRONG_FORMAT );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw new MessageException(CodeEnum.GL31004.getStatus(), WRONG_FORMAT );
                                }
                            } else {
                                throw new MessageException(CodeEnum.GL20008.getStatus(), WRONG_FORMAT );//错误地址
                            }
                        }
                        //添加validator的数据校验
                        String validation = PoiValidationUtil.validation(obj);
                        if (!StringUtils.isEmpty(validation)) {
                            throw new MessageException(CodeEnum.GL20008.getStatus(), validation + "," + new WrongIndex(sheetName, wrongY, null).toString() + obj.toString());//错误地址
                        }
                        if (!beans.add(obj)){
                            //不现实在那一列错误
//                        if (CodeEnum.GL20008.getStatus() == 20008) {
//                            continue;
//                        } else {
//                            throw new MessageException(CodeEnum.GL20008.getStatus(), REPEAT_VALUE + "," + new WrongIndex(sheetName, wrongY, null).toString());
//                        }
                            throw new MessageException(CodeEnum.GL20008.getStatus(), REPEAT_VALUE);
                        }
                    }
                }
            }

        }
        return new ArrayList<>(beans);
    }


    /**
     * 通过map存放对应下2月份页表头不正确，请正确上传标，后期就不再循环去查找
     * <p>
     * 如果对应的文件头只需要导出的时候显示，那么就将其进行排除
     *
     * @param fields
     * @param head
     * @return
     */
    public static Map<String, Integer> getHeadIndex(Field[] fields, Row head) {
        if (head == null) {
            return null;
        }
        Map<String, Integer> map = new HashMap<>();
        for (Field field : fields) {
            ImportAndExport export = field.getAnnotation(ImportAndExport.class);//当前的注解与
            for (int index = 0; index < head.getLastCellNum(); index++) {
                Cell cell = head.getCell(index);//获取单元格的数据
                if (export != null && export.desc().equals(PoiUtils.getStringValueFromCell(cell) == null ? "" : PoiUtils.getStringValueFromCell(cell).trim())) {
                    if (export.exportOrImport().equals(ExcelEnum.EXPORT)) {
                        continue;
                    }
                    map.put(field.toString(), index);
                }
            }
        }

        return map;

    }

    /**
     * 获取拥有ImportAndExport字段的属性
     *
     * @param clazz
     * @return
     */
    public static Field[] getFieldWithExportAnnotion(Class clazz) {
        Field[] declaredFields = clazz.getDeclaredFields();//获得字段
        List<Field> list = new ArrayList<>();
        for (Field field : declaredFields) {
            ImportAndExport export = field.getAnnotation(ImportAndExport.class);//当前的注解与
            if (export != null && !export.exportOrImport().equals(ExcelEnum.EXPORT)) {
                list.add(field);
            }
        }
        Field[] returnFiled = new Field[list.size()];
        for (int i = 0; i < list.size(); i++) {
            returnFiled[i] = list.get(i);//java.lang.Object; cannot be cast to [Ljava.lang.reflect.Field;
        }
        return returnFiled;
    }


    /**
     * 将数据转换为对象应有的格式
     *
     * @param classzz
     * @param value
     * @return
     */
    private static Object convertType(Class classzz, String value) {
        if (value == null || value.trim().equals("")) {
            return null;//如果value为null的直接返回
        } else if (Integer.class == classzz || int.class == classzz) {
            return Integer.valueOf(value);
        } else if (Short.class == classzz || short.class == classzz) {
            return Short.valueOf(value);
        } else if (Byte.class == classzz || byte.class == classzz) {
            return Byte.valueOf(value);
        } else if (Character.class == classzz || char.class == classzz) {
            return value.charAt(0);
        } else if (Long.class == classzz || long.class == classzz) {
            return Long.valueOf(value);
        } else if (Float.class == classzz || float.class == classzz) {
            return Float.valueOf(value);
        } else if (Double.class == classzz || double.class == classzz) {
            return Double.valueOf(value);
        } else if (Boolean.class == classzz || boolean.class == classzz) {
            return Boolean.valueOf(value.toLowerCase());
        } else if (BigDecimal.class == classzz) {
            return new BigDecimal(value);
        } else if (Date.class == classzz) {//时间类型转换
            DateTime parse = DateUtil.parse(value);
            return parse;
        }
        return value;
    }


    /**
     * 数据的正则判断
     *
     * @param pattern
     * @return
     */
    public static Boolean pattern(String pattern, String value, Boolean isNeed) {
        if (pattern == null && isNeed) {
            return false;
        }
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(value);
        return m.matches();
    }

    /**
     * 获得get和set方法
     */

    static class MethodUtils {
        private static final String SET_PREFIX = "set";
        private static final String GET_PREFIX = "set";

        private static String capitalize(String name) {
            if (name == null || name.length() == 0) {
                return name;
            }
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }

        public static String setMethodName(String propertyName) {
            return SET_PREFIX + capitalize(propertyName);
        }

        public static String getMethodName(String propertyName) {
            return GET_PREFIX + capitalize(propertyName);
        }

    }

    //1、错误地址提示类
    static class WrongIndex {
        Integer wrongX;//错误信息提示列
        Integer wrongY;//错误信息提示行
        String sheetName;//sheet页

        WrongIndex(String sheetName, Integer wrongY, Integer wrongX) {
            if (wrongX != null) {
                this.wrongX = wrongX + 1;
            }
            if (wrongY != null) {
                this.wrongY = wrongY + 1;
            }

            this.sheetName = sheetName;
        }

        @Override
        public String toString() {

            StringBuilder builder = new StringBuilder();
            builder.append("错误地址在：" + sheetName + "页");
            if (wrongY != null){
                builder.append(wrongY + "行 ");
            }
            if (wrongX != null){
                builder.append(wrongX + "列 ");
            }

            return builder.toString();
        }
    }

}
