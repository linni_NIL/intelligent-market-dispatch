package team.id.market.common.utils;

import team.id.market.common.excel.ImportAndExport;
import team.id.market.common.exception.MessageException;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by achims on 2019/7/31.
 * poi读取
 */
public class PoiUtils {
    public static final Integer DEFAULT_CELL_WIDTH = 2018;
    public static final String WRONG_SHEET = "页表头不正确，请正确上传";
    public static final String EMPTY_CELL = "数据为空";
    public static final String WRONG_FORMAT = "格式有误";
    public static final String REPEAT_VALUE = "重复数据";


    /**
     * @param cell
     * @return
     */
    public static String getStringValueFromCell(Cell cell) {
        SimpleDateFormat sFormat = new SimpleDateFormat("MM/dd/yyyy");
        DecimalFormat decimalFormat = new DecimalFormat("#.####");
        String cellValue = "";
        if (cell == null) {
            return null;
        } else if (cell.getCellType() == CellType.STRING) {
            cellValue = cell.getStringCellValue();
        } else if (cell.getCellType() == CellType.NUMERIC) {
            if (HSSFDateUtil.isCellDateFormatted(cell)) {// 处理日期格式、时间格式
                SimpleDateFormat sdf = null;
                if (cell.getCellStyle().getDataFormat() == HSSFDataFormat
                        .getBuiltinFormat("h:mm")) {
                    sdf = new SimpleDateFormat("HH:mm");
                } else {// 日期
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                }
                Date date = cell.getDateCellValue();
                cellValue = sdf.format(date);
            } else if (cell.getCellStyle().getDataFormat() == 58) {
                // 处理自定义日期格式：m月d日(通过判断单元格的格式id解决，id的值是58)
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                double value = cell.getNumericCellValue();
                Date date = org.apache.poi.ss.usermodel.DateUtil
                        .getJavaDate(value);
                cellValue = sdf.format(date);
            } else {
                Double value = cell.getNumericCellValue();
                CellStyle style = cell.getCellStyle();
                DecimalFormat format = new DecimalFormat();
                String temp = style.getDataFormatString();
                // 单元格设置成常规
                if (temp.equals("General")) {
                    format.applyPattern("#");
                    cellValue = Integer.valueOf(value.intValue()).toString();
                }
            }
//            if (HSSFDateUtil.isCellDateFormatted(cell)) {
//                double d = cell.getNumericCellValue();
//                Date date = HSSFDateUtil.getJavaDate(d);
//                cellValue = sFormat.format(date);
//            } else {
//                cellValue = decimalFormat.format((cell.getNumericCellValue()));
//            }
        } else if (cell.getCellType() == CellType.BLANK) {
            cellValue = "";
        } else if (cell.getCellType() == CellType.BOOLEAN) {
            cellValue = String.valueOf(cell.getBooleanCellValue());
        } else if (cell.getCellType() == CellType.ERROR) {
            cellValue = "";
        } else if (cell.getCellType() == CellType.FORMULA) {
            try {
                cellValue = decimalFormat.format((cell.getNumericCellValue()));//因为执行会出现精度缺失 所以使用decimal
            } catch (IllegalStateException e) {
                cellValue = String.valueOf(cell.getRichStringCellValue());
            }
        }
        return cellValue;
    }

    /**
     * 判断当前行是否为空
     *
     * @param row
     * @return
     */
    public static boolean isRowEmpty(Row row) {
        for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
            Cell cell = row.getCell(c);
            if (cell != null && StringUtils.isEmpty((cell + "").trim())) {
                continue;
            }
            if (cell != null && cell.getCellType() != CellType.BLANK)
                return false;//不是空行
        }
        return true;//是空行
    }


    /**
     * 读取数据不运行有空格或者空字符出现，如果出现的话需要提示
     *
     * @param cell
     * @param y
     * @param x
     * @return
     */
    public static String getStringValueFromCellWithoutEmpty(Cell cell, Integer y, Integer x) {
        String stringValueFromCell = getStringValueFromCell(cell);
        if (stringValueFromCell.trim().equals("")) {
            throw new MessageException(300, y + "行" + x + "列数据读取为空字符串");
        }
        return stringValueFromCell;
    }


}

class PoiValidationUtil {

    private final static Validator VALIDATOR;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        VALIDATOR = factory.getValidator();
    }

    public static String validation(Object obj) {
        Set<ConstraintViolation<Object>> set = null;
        set = VALIDATOR.validate(obj);
        if (set != null && set.size() > 0) {
            return getValidateErrMsg(set);
        }
        return null;
    }

    /**
     * 获取对应的字段信息
     *
     * @param set
     * @return
     */
    private static String getValidateErrMsg(Set<ConstraintViolation<Object>> set) {
        StringBuilder builder = new StringBuilder();
        for (ConstraintViolation<Object> constraintViolation : set) {
            Class cls = constraintViolation.getRootBean().getClass();
            String fieldName = constraintViolation.getPropertyPath().toString();
            List<Field> fields = new ArrayList<>(Arrays.asList(cls.getDeclaredFields()));
            Class superClass = cls.getSuperclass();
            if (superClass != null) {
                fields.addAll(Arrays.asList(superClass.getDeclaredFields()));
            }
            String name = null;
            for (Field field : fields) {
                if (field.getName().equals(fieldName) && field.isAnnotationPresent(ImportAndExport.class)) {
                    name = field.getAnnotation(ImportAndExport.class).desc();
                    break;
                }
            }
            if (name == null) {
                name = fieldName;
            }
            builder.append(name).append(constraintViolation.getMessage()).append(",");
        }
        return builder.substring(0, builder.length() - 1);
    }

}
