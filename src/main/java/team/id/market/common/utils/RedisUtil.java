package team.id.market.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.ConvertingCursor;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @Author: achims
 * @Create: 2020-06-21 17:10
 * @Description: redis工具类
 **/
@Slf4j
@Component
public class RedisUtil<T> {

    public static final String LOCK_PREFIX = "redis_lock:";
    public static final int LOCK_EXPIRE = 300; // ms


    public static final int TRY_TIME = 10;//10秒作为上线
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    //上锁脚本
    private static final String LOCK_LUA = "if redis.call('setnx', KEYS[1], ARGV[1]) == 1 then redis.call('expire', KEYS[1], ARGV[2]) return 'true' else return 'false' end";
    //解锁脚本
    private static final String UNLOCK_LUA = "if redis.call('get', KEYS[1]) == ARGV[1] then redis.call('del', KEYS[1]) end return 'true' ";
    private RedisSerializer<String> argsSerializer = new StringRedisSerializer();
    private RedisSerializer<String> resultSerializer = new StringRedisSerializer();

    /**
     * 判斷key值是否存在
     *
     * @return
     */
    public boolean isExist(String key) {
        Boolean aBoolean = redisTemplate.hasKey(key);
        return aBoolean;

    }
    /*****************************************redis的队列 pop 与push*******************************************************/
    /**
     * 取值 - <brpop：阻塞式> - 推荐使用
     *
     * @param key      键
     * @param timeout  超时时间
     * @param timeUnit 给定单元粒度的时间段
     *                 TimeUnit.DAYS          //天
     *                 TimeUnit.HOURS         //小时
     *                 TimeUnit.MINUTES       //分钟
     *                 TimeUnit.SECONDS       //秒
     *                 TimeUnit.MILLISECONDS  //毫秒
     * @return
     */
    public Object brpop(String key, long timeout, TimeUnit timeUnit) {

        try {
            return redisTemplate.opsForList().rightPop(key, timeout, timeUnit);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object brpop(String key) {

        try {
            return redisTemplate.opsForList().rightPop(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object blrpop(String key, long timeout, TimeUnit timeUnit) {
        try {
            return redisTemplate.opsForList().leftPop(key, timeout, timeUnit);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object blrpop(String key) {
        try {
            return redisTemplate.opsForList().leftPop(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 查看值
     *
     * @param key   键
     * @param start 开始
     * @param end   结束 0 到 -1代表所有值
     * @return
     */
    public List<Object> lrange(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


//    /**
//     * 添加redis的上锁时间
//     *
//     * @param key  上锁的key值
//     * @param time 上锁的时间
//     * @return
//     */
//    public boolean lock(String key, Integer time) {
//        String lock = LOCK_PREFIX + key;
//        // 利用lambda表达式
//
//        Boolean execute = (Boolean) redisTemplate.execute((RedisCallback) connection -> {
//            Boolean acquire = connection.setNX(lock.getBytes(), "lock".getBytes());
//            if (acquire) {
//                redisTemplate.expire(lock, time == null ? 100 : time, TimeUnit.SECONDS);
//                return true;
//            }
//            return false;
//        });
//        return execute;
//    }
//
//    public boolean lock(String key, Integer time, TimeUnit timeUnit) {
//        String lock = LOCK_PREFIX + key;
//        // 利用lambda表达式
//
//        Boolean execute = (Boolean) redisTemplate.execute((RedisCallback) connection -> {
//            Boolean acquire = connection.setNX(lock.getBytes(), "lock".getBytes());
//            if (acquire) {
//                redisTemplate.expire(lock, time == null ? 100 : time, timeUnit);
//                return true;
//            }
//            return false;
//        });
//        return execute;
//    }


    public boolean lock(String lockKey, String currentThread) {
        return this.lock(lockKey, currentThread, LOCK_EXPIRE);
    }

    /**
     * 原子性操作
     *
     * @Description 加锁
     * @Param [lockKey锁住的key, val key值，确保唯一, second 过期时间，防止死锁] 这里建议是当前操作的线程
     * @Return boolean
     */
    public boolean lock(String lockKey, String currentThread, int second) {
        return lock(lockKey, currentThread, second, null);
    }

    /**
     * 原子性操作
     *
     * @Description 加锁
     * @Param [lockKey锁住的key, val key值，确保唯一, second 过期时间，防止死锁] 这里建议是当前操作的线程
     * @Return boolean
     */
    public boolean lock(String lockKey, String currentThread, int aliveSecond, Integer tryTimeSecond) {
        long current = 0;
        if (tryTimeSecond == null) {
            current = new Date().getTime();//0.1秒
        } else if ((tryTimeSecond > 0 && tryTimeSecond < TRY_TIME)) {
            current = new Date().getTime() + tryTimeSecond * 100000;//0-10秒的重试
        } else {
            return false;
        }
        boolean lockValue = false;
        RedisScript lockRedisScript = RedisScript.of(LOCK_LUA, String.class);
        List<String> keys = Collections.singletonList(LOCK_PREFIX + lockKey);
        //小于当前时间 且lock为false的
        if (tryTimeSecond != null) {
            while (System.currentTimeMillis() < current && lockValue == false) {
                String flag = redisTemplate.execute(lockRedisScript, argsSerializer, resultSerializer, keys, currentThread, String.valueOf(aliveSecond));
                lockValue = Boolean.valueOf(flag);
            }
        } else {
            String flag = redisTemplate.execute(lockRedisScript, argsSerializer, resultSerializer, keys, currentThread, String.valueOf(aliveSecond));
            lockValue = Boolean.valueOf(flag);
        }


        return lockValue;
    }


    /**
     * 原子性操作
     *
     * @Description 加锁
     * @Param [lockKey锁住的key, val key值，确保唯一, second 过期时间，防止死锁] 这里建议是当前操作的线程
     * @Return boolean
     */
    public boolean lock(String lockKey, int second) {
        return lock(lockKey, Thread.currentThread().getName(), second);
//        RedisScript lockRedisScript = RedisScript.of(LOCK_LUA, String.class);
//        List<String> keys = Collections.singletonList(lockKey);
//        String flag = redisTemplate.execute(lockRedisScript, argsSerializer, resultSerializer, keys, Thread.currentThread().getName(), String.valueOf(second));
//        return Boolean.valueOf(flag);
    }


    /**
     * @Author achims
     * @Description 通过key和val值解锁，只有满足key值和val值相等才能解锁，防止被其他线程并发解锁
     * @Param [lockKey 被锁的key, val key值]
     * @Return void
     */
    public void unlock(String lockKey, String val) {
        RedisScript unLockRedisScript = RedisScript.of(UNLOCK_LUA, String.class);
        List<String> keys = Collections.singletonList(LOCK_PREFIX + lockKey);
        redisTemplate.execute(unLockRedisScript, argsSerializer, resultSerializer, keys, val);
    }

//
//    /**
//     * 删除过期
//     *
//     * @return
//     */
//    public boolean unLock(String key) {
//        return redisTemplate.delete(LOCK_PREFIX + key);
//    }
//
//    public Integer unLock(List<String> key) {
//
//        List<String> collect = key.stream().map(e -> LOCK_PREFIX + e).collect(Collectors.toList());
//        Long delete = redisTemplate.delete(collect);
//        return delete.intValue();
//    }

    public <T> Object getLockStr(String key) {
        return redisTemplate.opsForValue().get(LOCK_PREFIX + key);
    }

    /**
     * 设置key的过期时间
     *
     * @param key  键
     * @param time 时间(秒)
     * @return true成功 / false失败
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean expire(String key, long time, TimeUnit timeUnit) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, timeUnit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * 根据key获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 返回0代表为永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }


    /**
     * 根据score来指定set中数据的位置
     *
     * @param key
     * @param value
     * @param score
     * @return
     */
    public boolean zset(String key, Object value, Long score) {
        return redisTemplate.opsForZSet().add(key, value, score);
    }

    private static final String ZSET_LPOP_SCRIPT = "local result = redis.call('ZRANGE', KEYS[1], 0, 0)\n" +
            "local element = result[1]\n" +
            "if element then\n" +
            //标示由小到达取出第一个
            "    redis.call('ZREM', KEYS[1], element)\n" +
            "    return element\n" +
            "else\n" +
            "    return nil\n" +
            "end";


    /**
     * 使用zset获取弹出第一个数据
     *
     * @param key
     * @param resultClass
     * @param <T>
     * @return
     */
    public <T> T zgetLPop(String key, Class<T> resultClass) {
        return (T) redisTemplate.execute(new DefaultRedisScript<>(ZSET_LPOP_SCRIPT, resultClass), Collections.singletonList(key));
    }

    public <T> List zgetAllAsPop(String key, Class<T> resultClass) {
        List<T> list = new ArrayList<>();
        T value = null;
        do {
            value = zgetLPop(key, resultClass);
            if (Objects.nonNull(value)) {
                list.add(value);
            }
        }
        while (value != null);

        return list;
    }


    /**
     * 由小到大的排序值
     *
     * @param key
     * @param start
     * @param end
     * @param <T>
     * @return
     */
    public <T> Object zgetRange(String key, long start, long end) {
        return redisTemplate.opsForZSet().range(key, start, end);
    }

    public <T> Object zgetAll(String key) {
        return redisTemplate.opsForZSet().range(key, 0, -1);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true存在 / false不存在
     */
    public boolean hasKey(String key) {
        try {
            return redisTemplate.hasKey(key);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean mhasKey(String key, String innerKey) {
        return redisTemplate.opsForHash().hasKey(key, innerKey);
    }

    /**
     * 删除key
     *
     * @param key 可以传一个或多个
     */
    @SuppressWarnings("unchecked")
    public void del(String... key) {
        if (key != null && key.length > 0) {
            if (key.length == 1) {
                redisTemplate.delete(key[0]);
            } else {
                redisTemplate.delete(CollectionUtils.arrayToList(key));
            }
        }
    }


    public Long del(List<String> keys) {
        if (keys != null && keys.size() > 0) {
            return redisTemplate.delete(keys);
        }
        return 0L;
    }
    //============================String=============================

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public <T> Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 批量查询
     *
     * @param keys
     * @param <T>
     * @return
     */
    public <T> List<Object> multiGet(Collection<String> keys) {
        if (keys == null || keys.isEmpty()) {
            return null;
        }
        return redisTemplate.opsForValue().multiGet(keys);
    }


    /**
     * 注意 这里返回的是key的值
     *
     * @param pattern
     * @return
     * @throws IOException
     */
    public List<String> scanKeys(String pattern) throws IOException {
        long start = System.currentTimeMillis();
        ScanOptions options = ScanOptions.scanOptions()
                .count(10000) //这里指定每次扫描key的数量
                .match(pattern)
                .build();////需要匹配的key
        RedisSerializer<String> redisSerializer = (RedisSerializer<String>) redisTemplate.getKeySerializer();
        Cursor cursor = (Cursor) redisTemplate.executeWithStickyConnection(redisConnection -> new ConvertingCursor<>(redisConnection.scan(options), redisSerializer::deserialize));
        List<String> result = new ArrayList<>();
        while (cursor.hasNext()) {
            result.add(cursor.next().toString());
        }
        //切记这里一定要关闭，否则会耗尽连接数。
        cursor.close();
        log.info("scan扫描共耗时：{} ms key数量：{}", System.currentTimeMillis() - start, result.size());

        return result;

    }


    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 / false失败
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean multiSet(Map<String, Object> map) {
        try {
            redisTemplate.opsForValue().multiSet(map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true成功 / false失败
     */
    public boolean set(String key, Object value, Long time, TimeUnit timeUnit) {
        try {
            redisTemplate.opsForValue().set(key, value, time, timeUnit);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    /**
     * 普通缓存放入并设置过期时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) 如果time小于等于0 将设置无限期
     * @return true成功 / false失败
     */
    public boolean set(String key, Object value, long time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 递增
     *
     * @param key       键
     * @param increment 要增加几
     * @return
     */
    public long incr(String key, long increment) {
        if (increment < 0) {
            throw new RuntimeException("The increment must be greater than 0");
        }

        return redisTemplate.opsForValue().increment(key, increment);
    }

    /**
     * 递减
     *
     * @param key       键
     * @param decrement 要减少几
     * @return
     */
    public long decr(String key, long decrement) {
        if (decrement < 0) {
            throw new RuntimeException("The decrement must be greater than 0");
        }
        return redisTemplate.opsForValue().increment(key, -decrement);
    }

    //================================Map=================================

    /**
     * 获取hashKey某项对应的值
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return 值
     */
    public Object hget(String key, String item) {
        return redisTemplate.opsForHash().get(key, item);
    }


    /**
     * 获取hashKey对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> hmget(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    public List<Object> hmget(String key, List innerKeys) {
        return redisTemplate.opsForHash().multiGet(key, innerKeys);
    }

    /**
     * HashSet
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true成功 / false失败
     */
    public boolean hmset(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * HashSet 并设置时间
     *
     * @param key  键
     * @param map  对应多个键值
     * @param time 时间(秒) 如果time小于等于0 将设置无限期
     * @return true成功 / false失败
     */
    public boolean hmset(String key, Map<String, Object> map, long time) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向hash中放入数据,如果不存在将创建
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @return true成功 / false失败
     */
    public boolean hset(String key, String item, Object value) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向hash中放入数据,如果不存在将创建，同时可设置过期时间
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @param time  时间(秒)  注意:如果已存在的hash有时间,这里将会替换原有的时间
     * @return true成功  / false失败
     */
    public boolean hset(String key, String item, Object value, long time) {
        try {
            redisTemplate.opsForHash().put(key, item, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除hash中的值
     *
     * @param key  键 不能为null
     * @param item 项 可以是多个 不能为null
     */

    public void hdel(String key, List<String> item) {
        redisTemplate.executePipelined((new RedisCallback<Object>() {
            @Override
            public Object doInRedis(RedisConnection connection) {
                //业务操作
                if (!CollectionUtils.isEmpty(item)) {
                    for (String innerKey : item) {
                        connection.hDel(key.getBytes(), innerKey.getBytes());
                    }
                }
                return null;
            }
        }));
    }

    /**
     * 判断hash中是否存在该项
     *
     * @param key  键 不能为null
     * @param item 项 不能为null
     * @return true存在 / false不存在
     */
    public boolean hHasKey(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    /**
     * 递增hash中某项所对应的值 如果不存在,就会创建一个 并把递增后的值返回
     *
     * @param key       键
     * @param item      项
     * @param increment 要增加几
     * @return 该项递增后的值
     */
    public int hincr(String key, String item, int increment) {
        return redisTemplate.opsForHash().increment(key, item, increment).intValue();
    }

    /**
     * 递减hash中某项所对应的值 如果不存在,就会创建一个      if (increment < 0) {
     * throw new RuntimeException("The decrement must be greater than 0");
     * } 并把递减后的值返回
     *
     * @param key       键
     * @param item      项
     * @param decrement 要减少几
     * @return 该项递减后的值
     */
    public int hdecr(String key, String item, int decrement) {
        return redisTemplate.opsForHash().increment(key, item, -decrement).intValue();
    }

    //============================set=============================

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set sGet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 查询value是否存在于set中
     *
     * @param key   键
     * @param value 值
     * @return true存在 / false不存在
     */
    public boolean sHasKey(String key, Object value) {
        try {
            return redisTemplate.opsForSet().isMember(key, value);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据放入set
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 将数据放入set，同时可设置过期时间
     *
     * @param key    键
     * @param time   时间(秒) 如果time小于等于0 将设置无限期
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long sSetAndTime(String key, long time, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (time > 0) {
                expire(key, time);
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取set的长度
     *
     * @param key 键
     * @return
     */
    public long sGetSetSize(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 从set中移除value
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long setRemove(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().remove(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
    //===============================list=================================


    public List<Object> lGet(String key) {
        try {

            return redisTemplate.opsForList().range(key, 0, -1);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取list中的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束  注：0 到 -1代表所有值
     * @return
     */
    public List<Object> lGet(String key, long start, long end) {
        try {

            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 移除 指定 key 列表中的 start  和 end 元素
     *
     * @param key
     * @param start
     * @param end
     */
    public void lTrim(String key, long start, long end) {
        try {
            redisTemplate.opsForList().trim(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 获取list的长度
     *
     * @param key 键
     * @return
     */
    public long lGetListSize(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 通过索引 获取list中的值
     *
     * @param key   键
     * @param index 索引  index>=0时， 0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object lGetIndex(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将数据从右边放入list
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据放入list，同时可设置过期时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) 如果time小于等于0 将设置无限期
     * @return
     */
    public boolean lSet(String key, Object value, long time) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据批量放入list
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean lSet(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据批量放入list，同时可设置过期时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒)
     * @return
     */
    public boolean lSet(String key, List<Object> value, long time) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (time > 0) {
                expire(key, time);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean lUpdateIndex(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 从list中移除多个值为value的项
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long lRemove(String key, long count, Object value) {
        try {
            Long remove = redisTemplate.opsForList().remove(key, count, value);
            return remove;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

}
