package team.id.market.common.utils;

/**
 * @author 431587
 * @date 2023/6/14 14:16
 */
public final  class RegexpUtils {

    /*长度限制2~20字节，包含中文、字母、数字、下划线和减号且必须以字母和中文开头，全局唯一。大小写不做区分*/
    public static final String NAME_REGEXP = "^(?i)[a-z\\u4e00-\\u9fa5][a-z0-9_\\-\\u4e00-\\u9fa5]{1,19}$";


}
