package team.id.market.common.utils;


import org.apache.commons.codec.binary.Base64;
import org.apache.poi.util.IOUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.*;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class SerialNumberUtil {

    private static final String RSA = "RSA";
    public static final String RSA_ALGORITHM = "RSA";

    public static final String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAL7S0E9gJwxL-GU1H7xcfK_N24C96Iv_TeNdiK7YOV26j1bYLUCZpCti1RVIJ3R1x4K4CHS73Vy1iQ_aFLFG7dUCAwEAAQ";
    public static final String privateKey="MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAvtLQT2AnDEv4ZTUfvFx8r83bgL3oi_9N412Irtg5XbqPVtgtQJmkK2LVFUgndHXHgrgIdLvdXLWJD9oUsUbt1QIDAQABAkAFaa91API6nBEQteAPlMQIrJs0XK6zAevwrh-JbiK6cNq3AeNl8DfCBgNEW1NpiPdmcc3GCiuT7_hHL9IhehHBAiEA8dmpGMKlPRy7IhxLOo2pOLDIEZb1X4UkjGE0oy7TZsUCIQDJ_OTv4qwlwA4dO46nGj_3bAFJGYxuZ2VxBjcYXwhb0QIgY2qR9DevLRvuYOs4kW1CLqec1n4zh3x3ta0PsjTBefECIDr78NbkeKTlermqpLPDhJ56Gb4BCfNLmmHxELG7PrOhAiEAtS6QGqDxlha3AiZrjXXat1YiQHIqdroZSM-bsva7bcg";

//    public static final String publicKey="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCiBGs6LSbi9Onpe7IoDG38W9KDxxHTs/6aOs2/jwLRQdMWpKPq6ChpEB/QhSAPYw6OoMJpiOdqo0jyayhKvqPopxRv4RQmWWb6ISKXc0jt4E1IXuDT+09jHJJi839A+/U3SB3skw58D1TfYXbimsse7dfOnkKFSQKEb+FxkP4eGQIDAQAB";
//    public static final String privateKey="MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKIEazotJuL06el7sigMbfxb0oPHEdOz/po6zb+PAtFB0xako+roKGkQH9CFIA9jDo6gwmmI52qjSPJrKEq+o+inFG/hFCZZZvohIpdzSO3gTUhe4NP7T2MckmLzf0D79TdIHeyTDnwPVN9hduKayx7t186eQoVJAoRv4XGQ/h4ZAgMBAAECgYEAlxSZzPk3zuHlkjUnz9KvaWlSqFGlHQ42uClnnV7FmM7SAcZrmGz+6eIAWNkbKV4MwBHG2NSyP62eI3irbO/TNW9Y+B/ywJp/30LHBfBrVWcVzMtiEdwm7ghdigGg5+XrwsVYhH0uN5ApCbw5K5UxkWzyVWdC+1LtVFY9uE4jJ5UCQQDP08DjY9ICWzH5NtOogM+1k9P9z+qQIjfOWsdFMrqr/n2cK9gn7NCg72Q3kzfYVULhqMcEWUU0UwPlQbOUmdc/AkEAx5JakBOlvSZkIiW6iW7J+sXFScaoQRYt6w/mP3N0ZIhyrk6W5+jLAp7Lf5mdiahN/DVAxht07dr3Nl8FOERMpwJBALHU0cJBEdxuSE6lOWt//Tu0KLuQA3gdr37lcomlpNGzS6wsYCOZodNzxF9Bm5GDM12AW/s+n9bsCfNLyuwmz2UCQGHf//Q6/oSmaGNq13IBWKNZDOmiuO+6vehTUtt5dvK7DGs59b5kp/NwB2Nb5iwV07FGOK8v1ge05JrQvRGcRW8CQBUolmUUkbhYUhyTH2oVGarVFtoZ/mIuzjNA3dzThWsEIn12YSO8S4FLvlhz53TyeAPcc5+uTJwpdvJ43cnLxJA=";

    /**
     * 获取主板序列号，无法得到序列号
     * @return
     */
    public  String getMotherboardSN() {
        String result = "";
        try {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);

            String vbs = "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
                    + "Set colItems = objWMIService.ExecQuery _ \n"
                    + "   (\"Select * from Win32_BaseBoard\") \n"
                    + "For Each objItem in colItems \n"
                    + "    Wscript.Echo objItem.SerialNumber \n"
                    + "    exit for  ' do the first cpu only! \n" + "Next \n";

            fw.write(vbs);
            fw.close();
            String path = file.getPath().replace("%20", " ");
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + path);
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.trim();
    }
    /**
     * 获取CPU序列号
     * @return
     */
    public  String getCPUSerial() {
        String result = "";
        try {
            File file = File.createTempFile("tmp", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);
            String vbs = "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
                    + "Set colItems = objWMIService.ExecQuery _ \n"
                    + "   (\"Select * from Win32_Processor\") \n"
                    + "For Each objItem in colItems \n"
                    + "    Wscript.Echo objItem.ProcessorId \n"
                    + "    exit for  ' do the first cpu only! \n" + "Next \n";

            // + "    exit for  \r\n" + "Next";
            fw.write(vbs);
            fw.close();
            String path = file.getPath().replace("%20", " ");
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + path);
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
            file.delete();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        if (result.trim().length() < 1 || result == null) {
            result = "无CPU_ID被读取";
        }
        return result.trim();
    }

    /**
     * 获取盘符的的序列号
     * @param drive 打算安装在哪个盘符
     * @return
     */
    public  String getHardDiskSN(String drive) {
        String result = "";
        try {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);

            String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                    + "Set colDrives = objFSO.Drives\n"
                    + "Set objDrive = colDrives.item(\""
                    + drive
                    + "\")\n"
                    + "Wscript.Echo objDrive.SerialNumber"; // see note
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.trim();
    }
    /**
     * 获取widnows网卡的mac地址.
     */
    public  String getMAC_windows() {
        InetAddress ip ;
        NetworkInterface ni ;
        List<String> macList = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> netInterfaces = (Enumeration<NetworkInterface>) NetworkInterface
                    .getNetworkInterfaces();
            while (netInterfaces.hasMoreElements()) {
                ni = (NetworkInterface) netInterfaces.nextElement();
                // ----------特定情况，可以考虑用ni.getName判断
                // 遍历所有ip
                Enumeration<InetAddress> ips = ni.getInetAddresses();
                while (ips.hasMoreElements()) {
                    ip = (InetAddress) ips.nextElement();
                    if (!ip.isLoopbackAddress() // 非127.0.0.1
                            && ip.getHostAddress().matches("(\\d{1,3}\\.){3}\\d{1,3}")) {
                        macList.add(getMacFromBytes(ni.getHardwareAddress()));
                    }
                }
            }
        } catch (Exception e) {
            //logger.error("获取mac错误", e);
        }
        if (macList.size() > 0) {
            return macList.get(0);
        } else {
            return "";
        }
    }
    private  String getMacFromBytes(byte[] bytes) {
        StringBuffer mac = new StringBuffer();
        byte currentByte;
        for (byte b : bytes) {
            currentByte = (byte) ((b & 240) >> 4);
            mac.append(Integer.toHexString(currentByte));
            currentByte = (byte) (b & 15);
            mac.append(Integer.toHexString(currentByte));
        }
        return mac.toString().toUpperCase();
    }

    /**
     * 获取自定义机械号（哈希值）
     * @return
     */
    public static String machineCode(){
        SerialNumberUtil serialNumberUtil = new SerialNumberUtil();
        String code = serialNumberUtil.getCPUSerial()+"-"+serialNumberUtil.getHardDiskSN("c");
        String hashNumber = code.hashCode()+"";
        return hashNumber;
    }

    /**
     * 解密
     *
     * @author CPT
     * @date 2023/4/10 15:33
     * @param text 密文
     * @return 明文
     */
    public static String decrypt(String text, RSAPrivateKey privateKey) {
        try {
            // 获取一个Cipher对象，该对象可以执行各种加密和解密操作
            Cipher cipher = Cipher.getInstance(RSA_ALGORITHM);
            // 第一个参数决定执行解密操作
            // 第二参数使用私钥执行
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            // 将解密出来的内容转成字符串
            return new String(rsaSplitCodec(cipher, Cipher.DECRYPT_MODE, text.getBytes(StandardCharsets.UTF_8),
                    privateKey.getModulus().bitLength()));

        } catch (Exception  e) {
            return "解码失败";
        }
    }

    /**
     * 分割加密/解密
     *
     * @author CPT
     * @date 2023/4/10 15:29
     * @param cipher 加密器，可以是加密模式或解密模式。
     * @param opMode 操作模式，可以是加密模式或解密模式。
     * @param data 待加密或解密的数据。
     * @param keySize 密钥的长度。
     * @return 加密后密文
     */
    private static byte[] rsaSplitCodec(Cipher cipher, int opMode, byte[] data, int keySize) {
        // 计算最大分块大小：如果是解密模式，则需要先对数据进行Base64解码；根据操作模式和密钥长度计算出最大分块大小。
        int maxBlock = 0;
        if (opMode == Cipher.DECRYPT_MODE) {
            data = Base64.decodeBase64(data);
            maxBlock = keySize / 8;
        } else {
            maxBlock = keySize / 8 - 11;
        }
        // 初始化输出流和偏移量：准备一个字节数组输出流和偏移量变量。
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] buff;
        int i = 0;
        try {
            // 分块处理数据：使用cipher对数据进行分块加密或解密，分块大小为最大分块大小；将每个分块写入输出流。
            while (data.length > offSet) {
                if (data.length - offSet > maxBlock) {
                    buff = cipher.doFinal(data, offSet, maxBlock);
                } else {
                    buff = cipher.doFinal(data, offSet, data.length - offSet);
                }
                baos.write(buff, 0, buff.length);
                i++;
                offSet = i * maxBlock;
            }
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            throw new RuntimeException("解码失败");
        }
        // 返回处理后的数据：将输出流中的数据转换为字节数组并返回。
        byte[] resultData = baos.toByteArray();
        IOUtils.closeQuietly(baos);

        return resultData;
    }
    /**
     * 获取私钥
     *
     * @author CPT
     * @date 2023/4/10 14:05
     * @param privateKey 私钥
     * @return 私钥
     */
    public static RSAPrivateKey getPrivateKey(String privateKey) {
        try {
            // 获取密钥工厂实例
            KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
            // 将私钥转换为一种标准格式
            // 因为我们拿到密钥后进行URL安全的Base64编码，所以在使用时需要解析原本的内容
            PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(Base64.decodeBase64(privateKey));
            // 将上面的密钥规范转换成私钥对象，并返回
            return (RSAPrivateKey) keyFactory.generatePrivate(pkcs8EncodedKeySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
    }

}
