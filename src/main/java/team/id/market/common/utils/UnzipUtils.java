package team.id.market.common.utils;


import org.apache.commons.compress.archivers.sevenz.SevenZArchiveEntry;
import org.apache.commons.compress.archivers.sevenz.SevenZFile;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * @author 431587
 * @date 2023/5/23 14:27
 */
public class UnzipUtils {

    private static Logger logger = LoggerFactory.getLogger(UnzipUtils.class);

    private static String[] SUFFIX_NAMES = {".zip", ".rar", ".7z"};
    private static String ZIP = ".zip";
    private static String RAR = ".rar";
    private static String SEVEN_Z = ".7z";
    /**
     * 解压zip压缩文件到指定目录
     *
     * @param zipPath zip压缩文件绝对路径
     * @param descDir 指定的解压目录
     */
    public static void unzipFile(String zipPath, String descDir) throws IOException {
        try {
            File zipFile = new File(zipPath);
            if (!zipFile.exists()) {
                throw new IOException("要解压的压缩文件不存在");
            }
            File pathFile = new File(descDir);
            if (!pathFile.exists()) {
                pathFile.mkdirs();
            }
            InputStream input = new FileInputStream(zipPath);
            unzipWithStream(input, descDir);
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    /**
     * 解压
     *
     * @param inputStream
     * @param descDir
     */
    public static void unzipWithStream(InputStream inputStream, String descDir) {
        if (!descDir.endsWith(File.separator)) {
            descDir = descDir + File.separator;
        }
        try (ZipInputStream zipInputStream = new ZipInputStream(inputStream, Charset.forName("GBK"))) {
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                String zipEntryNameStr = zipEntry.getName();
                String zipEntryName = zipEntryNameStr;
                if (zipEntryNameStr.contains("/")) {
                    String str1 = zipEntryNameStr.substring(0, zipEntryNameStr.indexOf("/"));
                    zipEntryName = zipEntryNameStr.substring(str1.length() + 1);
                }
                String outPath = (descDir + zipEntryName).replace("\\\\", "/");
                int index = outPath.lastIndexOf('/');
                File outFile = null;
                if(index == -1){
                    outFile = new File(outPath.substring(0,outPath.lastIndexOf("\\")));
                }else {
                    outFile = new File(outPath.substring(0, outPath.lastIndexOf('/')));
                }

                if (!outFile.exists()) {
                    outFile.mkdirs();
                }
                if (new File(outPath).isDirectory()) {
                    continue;
                }
                writeFile(outPath, zipInputStream);
                zipInputStream.closeEntry();
            }
            logger.info("======解压成功=======");
        } catch (IOException e) {
            logger.warn("压缩包处理异常，异常信息{}" + e);
        }
    }

    //将流写到文件中
    public static void writeFile(String filePath, ZipInputStream zipInputStream) {
        try (OutputStream outputStream = new FileOutputStream(filePath)) {
            byte[] bytes = new byte[4096];
            int len;
            while ((len = zipInputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, len);
            }
        } catch (IOException ex) {
            logger.warn("解压文件时，写出到文件出错");
        }
    }

    /**
     * 解压缩文件到同一文件夹下
     * [.rar .zip .7z]
     *
     * @param compressedFilePath 压缩文件的路径，
     * @param targetPath         解压后保存的路径
     * @param suffix             压缩文件后缀名
     */
    public static String unCompressedFiles(String compressedFilePath, String targetPath, String suffix) throws IOException {
        String msg = "";
        if (!StringUtils.isEmpty(suffix)) {
            suffix = suffix.toLowerCase();
            if (ZIP.equals(suffix)) {
                 unzipFile(compressedFilePath, targetPath);
            }
            if (SEVEN_Z.equals(suffix)) {
                FileUtil1.unSevenZ(compressedFilePath, targetPath, true);
            }
        }
        return msg;
    }


    /**
     * 解压缩7z包
     *
     * @param rarFilePath
     * @param targetPath
     * @param isInSameDir
     * @return
     */
    public static String unSevenZ(String rarFilePath, String targetPath, boolean isInSameDir) {
        StringBuffer msg = new StringBuffer();
        try {
            SevenZFile sevenZFile = new SevenZFile(new File(rarFilePath));
            SevenZArchiveEntry entry = sevenZFile.getNextEntry();
            while (entry != null) {
                if (entry.isDirectory()) {
                    new File(targetPath + File.separator + entry.getName()).mkdirs();
                    entry = sevenZFile.getNextEntry();
                    continue;
                }
                String sevenZFileName = entry.getName();
                if (isInSameDir) {
                    if (sevenZFileName.indexOf("/") > -1) {
                        sevenZFileName = sevenZFileName.substring(sevenZFileName.lastIndexOf("/"), sevenZFileName.length());
                        if (sevenZFileName.startsWith("~$")) {
                            entry = sevenZFile.getNextEntry();
                            continue;
                        }
                    }
                }
                if (checkFileExistence(targetPath, sevenZFileName)) {
                    msg.setLength(0);
                    msg.append("存在重复的文件名：" + sevenZFileName);
                }
                FileOutputStream out = new FileOutputStream(targetPath
                        + File.separator + sevenZFileName);
                byte[] content = new byte[(int) entry.getSize()];
                sevenZFile.read(content, 0, content.length);
                out.write(content);
                out.close();
                entry = sevenZFile.getNextEntry();
                if (sevenZFileName.lastIndexOf(".") > -1) {
                    String suffix = sevenZFileName.substring(sevenZFileName.lastIndexOf("."), sevenZFileName.length()).toLowerCase();
                    if (Arrays.asList(SUFFIX_NAMES).contains(suffix)) {
                        String ret = unCompressedFiles(targetPath + File.separator + sevenZFileName, targetPath, suffix);
                        if (!StringUtils.isEmpty(ret)) {
                            msg.setLength(0);
                            msg.append(ret);
                        }
                    }
                }
            }
            sevenZFile.close();
            return msg.toString();
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage(), e);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return msg.toString();
    }

    /**
     * 判断解压的文件在解压目录下是否存在
     *
     * @param targetPath
     * @param zipFileName
     * @return
     */
    private static boolean checkFileExistence(String targetPath, String zipFileName) {
        boolean exist = false;
        File target = new File(targetPath);
        if (target.isDirectory()) {
            File[] files = target.listFiles();
            for (File file : files) {
                if (!file.isDirectory()) {
                    String fileName = file.getName();
                    if (fileName.equals(zipFileName)) {
                        exist = true;
                        break;
                    }
                }
            }
        }
        return exist;
    }


    private static String getFileName(String fileName){
        int nameLastIndex = fileName.lastIndexOf(".");
        String substring = fileName.substring(0, nameLastIndex);
        return substring;
    }


    //测试方法
    public static void main(String[] args) throws IOException {

//        String zipPath = "D:/test/test.7z";
//        String descDir = "D:/test/解压/";
//        unCompressedFiles(zipPath,descDir,".7z");
        String fileLocation ="E:\\IntelligentDispatch_Laser\\intelligentDispatch_Laser1\\laser_file\\test\\";
        //getFileInfo(fileLocation);

    }

}
