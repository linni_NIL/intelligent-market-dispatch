package team.id.market.common.utils;

import java.util.regex.Pattern;

public class ValidataUtil {
//    校验小车IP 校验IP格式
    public static boolean checkVehilceIp(String ip) {
        if (ip != null && !ip.isEmpty()) {
            String regex = "^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\."
                    + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
                    + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
                    + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$";
            boolean isValid = Pattern.matches(regex, ip);
            return isValid;
        }
        return false;
    }

//    校验小车名称 校验长度为2-20，可包含中文、字母、数字、下划线和减号，并且以字母开头的字符串
    public static boolean checkVehicleName(String vehicleName) {
        if (vehicleName != null && !vehicleName.isEmpty()) {
            String regex = "^[a-zA-Z\\u4e00-\\u9fa5][a-zA-Z0-9_\\-\\u4e00-\\u9fa5]{1,19}$";
            boolean isValid = Pattern.matches(regex, vehicleName);
            return isValid;
        }
        return false;
    }

    //    校验小车序列号 校验长度为22，至少包含一个字母和数字
    public static boolean checkVehicleSerial(String vehicleSerial) {
        if (vehicleSerial != null && !vehicleSerial.isEmpty()) {
            String regex = "^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{1,22}$";
            boolean isValid = Pattern.matches(regex, vehicleSerial);
            return isValid;
        }
        return false;
    }
}
