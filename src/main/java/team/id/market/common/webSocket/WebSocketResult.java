package team.id.market.common.webSocket;

import lombok.Data;


@Data
public class WebSocketResult {
    boolean connect = true;//是否连接
    String user;//当前连接用户
    String failedReason;//失败原因
}