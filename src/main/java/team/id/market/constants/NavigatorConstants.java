package team.id.market.constants;

import java.io.File;

/**
 * 导航模块常量定义
 */
public class NavigatorConstants {

    public final static String highQuality = "HIGH_QUALITY";

    public final static String lowQuality = "LOW_QUALITY";

    public final static String A_Z="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public final static String MODEL_FILE= File.separator+ "model_file"+File.separator;
}
