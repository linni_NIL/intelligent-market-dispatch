package team.id.market.navigation.constant;

import team.id.market.common.enums.CodeStatus;

/**
 * @author 431587
 * @date 2023/5/30 9:17
 */
public enum MapCodeEnum implements CodeStatus {
    POINT_LOCATION_REPEAT(31450,"点位坐标重复"),
    POINT_TYPE_EMPTY(31451,"点位类型为空"),
    POINT_NAME_REPEAT(31452,"点位名称重复"),
    POINT_NAME_VALIDATE_FAILED(31453,"点位名称校验失败"),
    POINT_DATA_VALIDATE_FAILED(31454,"点位数据校验失败"),

    POINT_ID_INDEX_VALIDATE_FAILED(31455,"点位IDindex校验失败"),

    LOCATION_NAME_VALIDATE_FAILED(31456,"站点名称校验失败"),
    LOCATION_NAME_REPEAT(31457,"站点名称重复"),

    POINT_TYPE_ERROR(31458,"站点类型错误"),

    POINT_ID_INDEX_IS_EMPTY(31459,"点位IDindex为空"),

    POINT_CARGO_ANGLE_VALIDATE_ERROR(31460,"CARGO_ANGLE校验失败"),

    POINT_MOTION_MODE_VALIDATE_ERROR(31461,"MOTION_MODE校验失败"),

    POINT_LOCATE_MODE_VALIDATE_ERROR(31462,"LOCATE_MODE校验失败"),

    POINT_STORKE_COLOR_VALIDATE_ERROR(31463,"颜色校验失败"),


    PATH_SRC_NULL(31550,"路径起点不存在"),
    PATH_DES_NULL(31551,"路径终点不存在"),

    PATH_NAME_VALIDATE_FAILED(31552,"路径名称校验失败"),
    PATH_NAME_REPEAT(31553,"路径名称重复"),

    BLOCK_POINT_VALIDATE_FAILED(31650,"块策略点校验失败"),
    BLOCK_TYPE_NULL(31551,"块策略类型为空"),

    BLOCK_NAME_VALIDATE_FAILED(31552,"块策略名称校验失败"),
    BLOCK_NAME_REPEAT(31553,"块策略名称重复"),
    BLOCK_POINT_REPEAT(31554,"块策略名称重复")

    ;



    private int status;
    private String msg;

    /**
     * 错误信息
     *
     * @return the string
     */
    @Override
    public String msg() {
        return msg;
    }

    @Override
    public Integer status() {
        return status;
    }

    MapCodeEnum(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
