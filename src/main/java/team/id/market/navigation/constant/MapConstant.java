package team.id.market.navigation.constant;

/**
 * @author 431587
 * @date 2023/5/24 16:02
 */
public class MapConstant {

    public static final String FILE_LOCATION =".\\laser_file\\";

    public static final String FILE_UPDATE_LOCATION ="update_file";



    public static final String PATH_INFO ="路径信息";

    public static final String POINT_INFO ="点位信息";

    public static final String BLOCK_INFO ="块策略信息";

    public static final String LOCATION_TYPE_INFO ="站点类型";

    public static final String VEHICLE_INFO ="车辆信息";

    public static final String base64Header = "data:image/png;base64,";

}
