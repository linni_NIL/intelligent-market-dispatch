package team.id.market.navigation.controller;

import team.id.market.common.result.CommonResult;
import team.id.market.common.result.PageBean;
import team.id.market.constants.NavigatorConstants;
import team.id.market.navigation.entity.dto.*;
import team.id.market.navigation.entity.simulation.dto.SimulationNavigateResultDTO;
import team.id.market.navigation.entity.simulation.vo.SimulationRouteVO;
import team.id.market.navigation.entity.vo.*;
import team.id.market.navigation.service.NavigateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import team.id.market.utils.OutUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

@RestController("MapController")
@RequestMapping("/pc/navigate/")
@Api(tags = "导航")
@Validated
public class NavigatorController {

    @Autowired
    private NavigateService navigateService;

    @GetMapping("/findShopByCommercialType")
    @ApiOperation(value = "根据业态查询店铺")
    public CommonResult<PageBean> findShopByCommercialType(@Validated findShopByCommercialTypeVo findShopByCommercialType){
        return navigateService.findShopByCommercialType(findShopByCommercialType);
    }

    @GetMapping("/findShopBySort")
    @ApiOperation(value = "根据排序查找店铺")
    public CommonResult<PageBean> findShopBySort(findShopBySortVo findShopBySort){
        //解压缩包，解析对应文件名称，保存文件及路径到数据库
        return navigateService.findShopBySort(findShopBySort);
    }

    @GetMapping("/getSimulationRoutes")
    @ApiOperation(value = "路线查询(模拟导航)")
    public CommonResult<SimulationNavigateResultDTO>  getRoutePlanning(@Validated SimulationRouteVO simulationRouteVO){
        //解压缩包，解析对应文件名称，保存文件及路径到数据库
        return navigateService.getSimulationRoutes(simulationRouteVO);

    }

    @GetMapping("/getAllFloor")
    @ApiOperation(value = "查询所有楼层")
    public CommonResult<List<String>> getAllFloor(String buildId){
        return navigateService.getAllFloor(buildId);
    }

    @GetMapping("/getAllCommercialType")
    @ApiOperation(value = "查询所有业态")
    public CommonResult<List<CommercialTypeInfoDto>>getAllCommercialType(String buildId){
        return navigateService.getAllCommercialType(buildId);
    }

    @GetMapping("/getMarketByName")
    @ApiOperation(value = "根据店铺名模糊查询")
    public CommonResult<PageBean>  getMarketByName(getMarketByNameVo getMarketByNameVo){
        return navigateService.getMarketByName(getMarketByNameVo);
    }

    @GetMapping("/getPictureById")
    @ApiOperation(value = "根据id查询图片")
    public void getPictureById(@Validated getPictureByIdVo getPictureByIdVo, HttpServletResponse response, HttpServletRequest req) throws Exception{
        CommonResult<MarketDocDto> result =  navigateService.getPictureById(getPictureByIdVo);
        String base64Img = "";
        if(getPictureByIdVo.getPictureType().equals(NavigatorConstants.highQuality)) {
            base64Img = result.getResult().getImgPreview();
        } else {
            base64Img = result.getResult().getImgPreviewCompress();
        }
        String end = ";base64,";
        int index = base64Img.indexOf(end);
        String type = base64Img.substring(0, index);
        base64Img = base64Img.substring(index + end.length());
        OutUtil.outStreamBytes(Base64.getDecoder().decode(base64Img),response,type);
    }

    @GetMapping("/getModelByFloor")
    @ApiOperation(value = "更具楼层查询模型")
    public void getModelByFloor(getModelByFloorVo getFloorVoByFloor,HttpServletResponse response, HttpServletRequest req) throws IOException, IOException {
        byte[] modelByFloor = navigateService.getModelByFloor(getFloorVoByFloor);
        OutUtil.outStreamBytes1(modelByFloor,response);
    }



























}
