package team.id.market.navigation.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommercialTypeInfo {

  private long id;
  private long commercialTypeId;
  private String commercialTypeNameSc;
  private String commercialTypeNameTc;
  private String commercialTypeNameEi;
  private java.sql.Timestamp createTime;
  private String buildId;



}
