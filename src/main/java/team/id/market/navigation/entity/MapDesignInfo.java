package team.id.market.navigation.entity;


import lombok.Data;

@Data
public class MapDesignInfo {

  private long uid;
  private String buildId;
  private String floor;
  private String marketId;
  private String designSerial;
  private java.sql.Timestamp createTime;
  private java.sql.Timestamp updateTime;
  private String createUser;
  private String modifyUser;



}
