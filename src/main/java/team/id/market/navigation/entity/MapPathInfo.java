package team.id.market.navigation.entity;


import team.id.market.common.excel.ImportAndExport;
import lombok.Data;

import java.util.Date;
import java.util.Objects;

@Data
public class MapPathInfo {

  @ImportAndExport(desc = "id")
  private Integer id;

  @ImportAndExport(desc = "序号")
  private Integer idIndex;

  @ImportAndExport(desc = "hash值")
  private String hashCode;

  @ImportAndExport(desc = "路网设计序列号")
  private String designSerial;

  @ImportAndExport(desc = "起点名称")
  private String srcPointName;

  @ImportAndExport(desc = "终点名称")
  private String desPointName;

  @ImportAndExport(desc = "路径开销")
  private long routingCount;

  @ImportAndExport(desc = "路径名")
  private String pathName;

  @ImportAndExport(desc = "更新时间")
  private Date updateTime;

  @ImportAndExport(desc = "是否删除")
  private Integer isDelete;

  @ImportAndExport(desc = "路径颜色")
  private String strokeColor;

  @ImportAndExport(desc = "起点x坐标")
  private String sourceX;

  @ImportAndExport(desc = "起点y坐标")
  private String sourceY;

  @ImportAndExport(desc = "终点x坐标")
  private String targetX;

  @ImportAndExport(desc = "终点y坐标")
  private String targetY;


}
