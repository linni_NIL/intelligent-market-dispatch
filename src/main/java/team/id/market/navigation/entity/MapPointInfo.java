package team.id.market.navigation.entity;


import team.id.market.common.excel.ImportAndExport;
import lombok.Data;

import java.util.Date;
import java.util.Objects;

@Data
public class MapPointInfo {
  @ImportAndExport(desc = "id")
  private String id;

  @ImportAndExport(desc = "点位id")
  private String pointID;

  @ImportAndExport(desc = "hash值")
  private String hashCode;

  @ImportAndExport(desc = "路网设计序列号")
  private String designSerial;

  @ImportAndExport(desc = "x坐标")
  private String xLocation;

  @ImportAndExport(desc = "y坐标")
  private String yLocation;

  @ImportAndExport(desc = "点类型")
  private String pointType;

  @ImportAndExport(desc = "店铺id")
  private String marketId;

  @ImportAndExport(desc = "创建时间")
  private Date creatTime;

  @ImportAndExport(desc = "更新时间")
  private Date updateTime;

  @ImportAndExport(desc = "是否删除")
  private Integer isDelete;

  @ImportAndExport(desc = "'修改用户'")
  private String modifyUser;
  @ImportAndExport(desc = "'点位描述'")
  private String pointDescription;

  @ImportAndExport(desc = "'点位名称'")
  private String pointName;


}
