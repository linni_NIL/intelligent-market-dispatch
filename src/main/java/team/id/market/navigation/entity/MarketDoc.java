package team.id.market.navigation.entity;


import lombok.Data;

@Data
public class MarketDoc {

  private long id;
  private String marketId;
  private String imgLocation;
  private String imgPreview;
  private String imgPreviewCompress;



}
