package team.id.market.navigation.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import team.id.market.common.excel.ImportAndExport;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketInfo {

    @ImportAndExport(desc = "'店铺id，唯一'")
    private String marketId;

    @ImportAndExport(desc = "'店铺名（简体）'")
    private String marketNameSc;

    @ImportAndExport(desc = "'位置'")
    private String marketLocation;

    @ImportAndExport(desc = "'点赞数'")
    private String marketLike;

    @ImportAndExport(desc = "'所属建筑物'")
    private String buildId;

    @ImportAndExport(desc = "''楼层''")
    private String floor;

    @ImportAndExport(desc = "'类别'")
    private String commercialType;

    @ImportAndExport(desc = "'音译'")
    private String transliteration;

    @ImportAndExport(desc = "'图片位置'")
    private String imgLocation;

    @ImportAndExport(desc = "'图片内容'")
    private String imgPreview;

    @ImportAndExport(desc = "'创建时间'")
    private String createTime;


    @ImportAndExport(desc = "'更新时间'")
    private String updateTime;

    @ImportAndExport(desc = "'创建用户'")
    private String createUser;

    @ImportAndExport(desc = "'修改用户'")
    private String modifyUser;

    @ImportAndExport(desc = "'是否删除'")
    private String isDelete;

    @ImportAndExport(desc = "'店铺名（繁体）'")
    private String marketNameTc;

    @ImportAndExport(desc = "'店铺名（英文）'")
    private String marketNameEi;

}
