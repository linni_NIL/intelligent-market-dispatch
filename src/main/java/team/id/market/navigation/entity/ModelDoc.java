package team.id.market.navigation.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelDoc {
  private Long id;
  private String buildId;
  private String floor;
  private String folder;
  private Date createTime;
  private Date updateTime;
  private Integer isDelete;



}
