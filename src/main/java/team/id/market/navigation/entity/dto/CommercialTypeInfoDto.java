package team.id.market.navigation.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommercialTypeInfoDto {

    private long commercialTypeId;
    private String commercialTypeNameSc;
    private String commercialTypeNameTc;
    private String commercialTypeNameEi;

}
