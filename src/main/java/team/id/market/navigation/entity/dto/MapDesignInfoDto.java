package team.id.market.navigation.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 431587
 * @date 2023/4/25 9:39
 */
@Data
public class MapDesignInfoDto {
    @ApiModelProperty(value = "路网关联地图id")
    private String mapId;

    @ApiModelProperty(value = "路网设计序列号")
    private String designSerial;

    @ApiModelProperty(value = "创建时间")
    private java.sql.Date createTime;

    @ApiModelProperty(value = "修改时间")
    private java.sql.Date updateTime;

    @ApiModelProperty(value = "是否删除")
    private long isDelete;

    @ApiModelProperty(value = "创建用户")
    private String createUser;

    @ApiModelProperty(value = "是否使用")
    private long isUsed;
}
