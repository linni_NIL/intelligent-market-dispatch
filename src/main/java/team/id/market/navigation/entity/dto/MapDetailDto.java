package team.id.market.navigation.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author 431587
 * @date 2023/4/24 18:07
 */
@Data
@Builder
public class MapDetailDto {


    @ApiModelProperty(value = "地图id")
    private String mapId;

    @ApiModelProperty(value = "地图名称")
    private String mapName;

    @ApiModelProperty(value = "压缩包名")
    private String zipName;

    @ApiModelProperty(value = "二维码地图名")
    private String orMapName;

    @ApiModelProperty(value = "激光地图Txt名")
    private String laserTxtName;

    @ApiModelProperty(value = "激光地图预览名")
    private String laserPreviewName;

    @ApiModelProperty(value = "激光地图PCD名")
    private String laserPcdName;

    @ApiModelProperty(value = "路网节点名")
    private String designMapLocation;

    @ApiModelProperty(value = "创建时间")
    private String createTime;

    @ApiModelProperty(value = "更新时间")
    private String updateTime;

    @ApiModelProperty(value = "关联AGV数量")
    private Integer relevanceVehicleNumber;

    @ApiModelProperty(value = "设备数量")
    private Integer equipmentNumber;

    @ApiModelProperty(value = "库位数量")
    private Integer cargoNumber;


    @ApiModelProperty(value = "是否删除")
    private Integer isdelete;


    @ApiModelProperty(value = "块策略数量")
    private Integer blockCount;

    @ApiModelProperty(value = "普通节点数量")
    private Integer pointCount;

    @ApiModelProperty(value = "使用状态")
    private Integer mapState;

    @ApiModelProperty(value = "上次加载时间")
    private String lastLoadTime;


}
