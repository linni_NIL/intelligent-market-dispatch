package team.id.market.navigation.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MapItemsDTO {
    //'地图id'
    private String mapId;
    //'地图名称'
    private String mapName;
}
