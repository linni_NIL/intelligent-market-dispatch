package team.id.market.navigation.entity.dto;

import team.id.market.common.excel.ImportAndExport;

import java.util.Date;

public class MapPathDto {


    @ImportAndExport(desc = "序号")
    private Integer idIndex;

    @ImportAndExport(desc = "hash值")
    private String hashCode;

    @ImportAndExport(desc = "路网设计序列号")
    private String designSerial;

    @ImportAndExport(desc = "路径名")
    private String pathName;

    @ImportAndExport(desc = "路径颜色")
    private String strokeColor;

    @ImportAndExport(desc = "起点x坐标")
    private String sourceX;

    @ImportAndExport(desc = "起点y坐标")
    private String sourceY;

    @ImportAndExport(desc = "终点x坐标")
    private String targetX;

    @ImportAndExport(desc = "终点y坐标")
    private String targetY;

}
