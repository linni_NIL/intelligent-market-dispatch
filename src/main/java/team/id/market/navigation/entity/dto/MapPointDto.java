package team.id.market.navigation.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import team.id.market.common.excel.ImportAndExport;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MapPointDto {

    @ImportAndExport(desc = "点位id")
    private String pointId;

    @ImportAndExport(desc = "'点位名称'")
    private String pointName;

    @ImportAndExport(desc = "hash值")
    private String hashCode;

    @ImportAndExport(desc = "x坐标")
    private String xLocation;

    @ImportAndExport(desc = "y坐标")
    private String yLocation;

    @ImportAndExport(desc = "点类型")
    private String pointType;

}
