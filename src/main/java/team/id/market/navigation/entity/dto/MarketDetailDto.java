package team.id.market.navigation.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import team.id.market.common.excel.ImportAndExport;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MarketDetailDto {

    @ApiModelProperty(value = "店铺id")
    private String marketId;

    @ApiModelProperty(value = "店铺名(简中)")
    private String marketNameSc;

    @ApiModelProperty(value = "店铺名（繁体）")
    private String marketNameTc;

    @ApiModelProperty(value = "店铺名（英文）")
    private String marketNameEi;

    @ApiModelProperty(value = "点赞数")
    private String marketLike;

    @ApiModelProperty(value = "楼层")
    private String floor;

    @ApiModelProperty(value = "类别")
    private String commercialType;

    @ApiModelProperty(value = "位置")
    private String marketLocation;

    @ApiModelProperty(value = "图片内容")
    private String imgPreview;





}
