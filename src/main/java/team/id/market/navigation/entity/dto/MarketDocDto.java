package team.id.market.navigation.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MarketDocDto {

    @ApiModelProperty(value = "店铺id")
    private String marketId;

    @ApiModelProperty(value = "图片内容(未压缩)")
    private String imgPreview;

    @ApiModelProperty(value = "图片内容（压缩）")
    private String imgPreviewCompress;

}
