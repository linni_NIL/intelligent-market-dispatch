package team.id.market.navigation.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import team.id.market.common.excel.ImportAndExport;

public class MarketInfoDto {

    @ApiModelProperty(value = "'店铺id，唯一'")
    private String marketId;

    @ApiModelProperty(value = "'店铺名'")
    private String marketName;

    @ApiModelProperty(value = "'位置'")
    private String marketLocation;

    @ApiModelProperty(value = "'点赞数'")
    private String marketLike;

    @ApiModelProperty(value = "'所属建筑物'")
    private String buildId;

    @ApiModelProperty(value = "''楼层''")
    private String floor;

    @ApiModelProperty(value = "'类别'")
    private String commercialType;

    @ApiModelProperty(value = "'音译'")
    private String transliteration;

    @ApiModelProperty(value = "'图片内容'")
    private String imgPreview;

}
