package team.id.market.navigation.entity.dto;

import lombok.Data;
import team.id.market.navigation.entity.MapPathInfo;
import team.id.market.navigation.entity.MapPointInfo;

import java.util.List;

@Data
public class NavigateResultDTO {

    /**
     *点位集合
     */
    List<MapPointDto> pointInfoList;

    /**
     *路径集合
     */
    List<MapPathDto> pathInfoList;

    /**
     *店铺信息
     */
    MarketInfoDto marketInfoDto;




}
