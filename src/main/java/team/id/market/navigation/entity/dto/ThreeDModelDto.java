package team.id.market.navigation.entity.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ThreeDModelDto {
    @ApiModelProperty(value = "楼层id")
    private String floor;

    // TODO: 2023/10/30 模型类型待定
    @ApiModelProperty(value = "三维模型")
    private Byte[] model;

}
