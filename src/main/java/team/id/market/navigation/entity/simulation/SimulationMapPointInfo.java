package team.id.market.navigation.entity.simulation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import team.id.market.common.excel.ImportAndExport;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimulationMapPointInfo {

    private String pointID;

    private String pointName;

    private String floor;

    private String xLocation;

    private String yLocation;

    private String pointType;

    private String linkPassingInfo;
}
