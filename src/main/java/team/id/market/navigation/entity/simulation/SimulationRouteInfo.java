package team.id.market.navigation.entity.simulation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimulationRouteInfo {
    private String sourcePointName;

    private String targetPointName;

    private List<SimulationMapPointInfo> routeList;
}
