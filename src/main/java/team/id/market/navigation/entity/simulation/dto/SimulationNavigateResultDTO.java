package team.id.market.navigation.entity.simulation.dto;

import lombok.Data;
import team.id.market.navigation.entity.simulation.SimulationMapPointInfo;
import team.id.market.navigation.entity.simulation.SimulationRouteInfo;

import java.util.List;

@Data
public class SimulationNavigateResultDTO {
    List<SimulationMapPointInfo> simulationMapPointInfos;

    SimulationRouteInfo simulationRouteInfo;
}
