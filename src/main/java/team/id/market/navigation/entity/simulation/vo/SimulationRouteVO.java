package team.id.market.navigation.entity.simulation.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SimulationRouteVO {
    /**
     *起点id
     */
    private String sourcePointId;
    /**
     *目标店铺id
     */
    @NotBlank
    private String targetMarketId;
}
