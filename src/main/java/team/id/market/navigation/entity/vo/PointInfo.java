package team.id.market.navigation.entity.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PointInfo {
    /**
     *楼层
     */
    private Integer floor;

    /**
     *点位id
     */
    private String pointId;

}
