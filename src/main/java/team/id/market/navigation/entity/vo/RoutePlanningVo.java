package team.id.market.navigation.entity.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class RoutePlanningVo {

    /**
     *建筑物id
     */
    private String buildId;
    /**
     *当前位置
     */
    private  PointInfo currentPostion;

    /**
     *目标位置
     */
    private  PointInfo targetPostion;




}
