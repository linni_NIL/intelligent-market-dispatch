package team.id.market.navigation.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author 431587
 * @date 2023/6/1 16:18
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class findShopByCommercialTypeVo {

    /**
     *业态
     */
    private String commercialType;

    /**
     *建筑物id
     */
    @NotBlank
    private String buildId;
    /**
     *楼层
     */
    private String floor;

    /**
     *总页数
     */
    private Integer pageSize;
    /**
     *当前页
     */
    private Integer currentPage;

}
