package team.id.market.navigation.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 431587
 * @date 2023/6/1 16:22
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class findShopBySortVo {

    /**
     *楼层
     */
    private String floor;

    /**
     *查询范围
     */
    private String queryRange;

    /**
     *总页数
     */
    private Integer pageSize;
    /**
     *当前页
     */
    private Integer currentPage;

    /**
     *建筑物id
     */
    private String buildId;
}
