package team.id.market.navigation.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 431587
 * @date 2023/11/13 18:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class getMarketByNameVo {

    /**
     *地图名
     */
    private String mapName;
    /**
     *建筑物id
     */
    private String buildId;
}

