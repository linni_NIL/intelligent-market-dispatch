package team.id.market.navigation.entity.vo;

import lombok.Data;

/**
 * @author 431587
 * @date 2023/11/13 18:38
 */
@Data
public class getModelByFloorVo {

    /**
     *楼层
     */
    private String floor;

    /**
     *建筑物id
     */
    private String buildId;
}
