package team.id.market.navigation.entity.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author 431587
 * @date 2023/11/13 18:34
 */
@Data
public class getPictureByIdVo {

    /**
     *店铺id
     */
    @NotBlank
    private String marketId;

    /**
     *建筑物id
     */
    @NotBlank
    private String buildId;

    /**
     *图片质量
     */
    @NotBlank
    private String pictureType;
}
