package team.id.market.navigation.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import team.id.market.navigation.entity.CommercialTypeInfo;

import java.util.List;

@Mapper
public interface CommercialTypeInfoMapper {

    List<CommercialTypeInfo> getAllCommercialType(@Param("buildId") String buildId);
}
