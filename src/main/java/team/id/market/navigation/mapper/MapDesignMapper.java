package team.id.market.navigation.mapper;

import org.apache.ibatis.annotations.Mapper;
import team.id.market.navigation.entity.MapDesignInfo;

import java.util.List;

@Mapper
public interface MapDesignMapper {

    List<MapDesignInfo> getAllDesignSerial(String buildingId);

}
