package team.id.market.navigation.mapper;

import org.apache.ibatis.annotations.Mapper;
import team.id.market.navigation.entity.MapPathInfo;

import java.util.List;

@Mapper
public interface MapPathMapper {
    List<MapPathInfo> getAllMapPathBySerial(String designSerial);
}
