package team.id.market.navigation.mapper;


import org.apache.ibatis.annotations.Mapper;
import team.id.market.navigation.entity.MapPointInfo;

import java.util.List;

@Mapper
public interface MapPointMapper {

    List<MapPointInfo> getAllMapPointBySerial(String designSerial);

    MapPointInfo getPointByMarketId(String marketId);

    MapPointInfo getPointByPointId(String pointId);
}
