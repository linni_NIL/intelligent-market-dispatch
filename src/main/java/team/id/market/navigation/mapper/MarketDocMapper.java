package team.id.market.navigation.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import team.id.market.navigation.entity.MarketDoc;

@Mapper
public interface MarketDocMapper {

    MarketDoc getHighPictureById(@Param("marketId")  String marketId,@Param("buildId") String buildId);

    MarketDoc getLowPictureById(@Param("marketId")  String marketId,@Param("buildId") String buildId);
}
