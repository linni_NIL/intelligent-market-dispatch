package team.id.market.navigation.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import team.id.market.navigation.entity.MarketInfo;
import team.id.market.setting.entity.ChargeInfo;

import java.util.List;

@Mapper
public interface MarketMapper {

    List<MarketInfo> findShopBySortVo(@Param("floor") String floor,@Param("buildId") String buildId);

    List<MarketInfo> findShopByCommercialType(@Param("commercialType") String commercialType,@Param("floor") String floor,@Param("buildId") String buildId);

    List<String> findAllFloor(String buildId);

    List<String> getAllCommercialType();

    List<MarketInfo> getMarketByName(@Param("marketName") String marketName,@Param("buildId") String buildId);

    MarketInfo getMarketById(String marketId);


}
