package team.id.market.navigation.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import team.id.market.navigation.entity.CommercialTypeInfo;
import team.id.market.navigation.entity.ModelDoc;

import java.util.List;

@Mapper
public interface ModelDocMapper {

    ModelDoc getModelDocByBuildAndFloor(@Param("buildId") String buildId, @Param("floor") String floor);
}
