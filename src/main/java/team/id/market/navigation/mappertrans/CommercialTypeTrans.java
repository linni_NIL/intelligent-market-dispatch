package team.id.market.navigation.mappertrans;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import team.id.market.navigation.entity.CommercialTypeInfo;
import team.id.market.navigation.entity.MarketDoc;
import team.id.market.navigation.entity.dto.CommercialTypeInfoDto;
import team.id.market.navigation.entity.dto.MarketDocDto;

@Mapper
public interface CommercialTypeTrans {
    CommercialTypeTrans INSTANCE =  Mappers.getMapper(CommercialTypeTrans.class);

    CommercialTypeInfoDto toCommercialTypeInfoDto(CommercialTypeInfo commercialTypeInfo);
}
