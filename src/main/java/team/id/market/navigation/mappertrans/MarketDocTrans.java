package team.id.market.navigation.mappertrans;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import team.id.market.navigation.entity.MarketDoc;
import team.id.market.navigation.entity.MarketInfo;
import team.id.market.navigation.entity.dto.MarketDetailDto;
import team.id.market.navigation.entity.dto.MarketDocDto;

@Mapper
public interface MarketDocTrans {
    MarketDocTrans INSTANCE =  Mappers.getMapper(MarketDocTrans.class);

    MarketDocDto toMarketDocDto(MarketDoc marketDoc);
}
