package team.id.market.navigation.mappertrans;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import team.id.market.navigation.entity.MarketInfo;
import team.id.market.navigation.entity.dto.MarketDetailDto;

@Mapper
public interface MarketTrams {
    MarketTrams INSTANCE =  Mappers.getMapper(MarketTrams.class);

    MarketDetailDto toMarketDetailDto(MarketInfo marketInfo);
}
