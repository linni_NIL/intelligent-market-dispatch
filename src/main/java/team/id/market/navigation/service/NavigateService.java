package team.id.market.navigation.service;

import team.id.market.common.result.CommonResult;
import team.id.market.common.result.PageBean;
import team.id.market.navigation.entity.dto.*;
import team.id.market.navigation.entity.simulation.dto.SimulationNavigateResultDTO;
import team.id.market.navigation.entity.simulation.vo.SimulationRouteVO;
import team.id.market.navigation.entity.vo.*;

import java.io.IOException;
import java.util.List;


public interface NavigateService {

    CommonResult<PageBean>  findShopBySort(findShopBySortVo findShopBySortVo);

    CommonResult<PageBean> findShopByCommercialType(findShopByCommercialTypeVo findShopByCommercialTypeVo);

    CommonResult<NavigateResultDTO>  getRoutePlanning(RoutePlanningVo routePlanning);

    CommonResult<List<String>> getAllFloor(String buildId);

    CommonResult<List<CommercialTypeInfoDto>>  getAllCommercialType(String buildId);

    CommonResult<PageBean>  getMarketByName(getMarketByNameVo getMarketByNameVo);

    CommonResult<MarketDocDto>  getPictureById(getPictureByIdVo getPictureByIdVo);

    byte[] getModelByFloor(getModelByFloorVo getFloorVoByFloor) throws IOException;

    /**
     * 模拟导航获取路线方法
     * */
    CommonResult<SimulationNavigateResultDTO>  getSimulationRoutes(SimulationRouteVO simulationRouteVO);
}
