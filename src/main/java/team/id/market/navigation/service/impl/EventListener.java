package team.id.market.navigation.service.impl;

import team.id.market.common.notify.NotifyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author 431587
 * @date 2023/6/20 14:44
 */
@Component
public class EventListener  implements ApplicationListener<NotifyEvent> {
    @Override
    public void onApplicationEvent(NotifyEvent event) {
        System.out.println(event.toString());
    }
}
