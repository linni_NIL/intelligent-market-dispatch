package team.id.market.navigation.service.impl;


import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Value;
import team.id.market.common.jgrapht.DijkstraRouter;
import team.id.market.common.result.CommonResult;
import team.id.market.common.result.PageBean;

import team.id.market.constants.NavigatorConstants;

import team.id.market.navigation.entity.*;
import team.id.market.navigation.entity.dto.*;
import team.id.market.navigation.entity.simulation.dto.SimulationNavigateResultDTO;
import team.id.market.navigation.entity.simulation.vo.SimulationRouteVO;
import team.id.market.navigation.entity.vo.*;
import team.id.market.navigation.mapper.*;
import team.id.market.navigation.mappertrans.CommercialTypeTrans;
import team.id.market.navigation.mappertrans.MarketDocTrans;
import team.id.market.navigation.mappertrans.MarketTrams;
import team.id.market.navigation.service.NavigateService;
import team.id.market.setting.mapper.SystemMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.DeflaterOutputStream;

import static team.id.market.common.result.PageBean.restPage;
import static team.id.market.constants.NavigatorConstants.A_Z;
import static team.id.market.constants.NavigatorConstants.MODEL_FILE;

@Service
public class NavigateServiceImpl implements NavigateService {


    private static Logger logger = LoggerFactory.getLogger(NavigateServiceImpl.class);

    @Autowired
    private SystemMapper systemMapper;

    @Autowired
    private MarketMapper marketMapper;

    @Autowired
    private MarketDocMapper marketDocMapper;

    @Autowired
    private CommercialTypeInfoMapper commercialTypeInfoMapper;

    @Autowired
    private ModelDocMapper modelDocMapper;

    @Autowired
    private DijkstraRouter dijkstraRouter;

    @Autowired
    private MapPointMapper mapPointMapper;

    private final static String highQuality = "HIGH_QUALITY";

    private final static String lowQuality = "LOW_QUALITY";

    @Value("${map.simulative.pc.position}")
    private String pc;

    @Override
    public CommonResult<PageBean> findShopBySort(findShopBySortVo findShopBySortVo){
        ArrayList<MarketDetailDto> marketDetailDtos = new ArrayList<>();
        Integer currentPage = findShopBySortVo.getCurrentPage();
        Integer pageSize = findShopBySortVo.getPageSize();
        if(currentPage != null && pageSize != null){
            PageHelper.startPage(currentPage,pageSize);
        }
        String queryRange = findShopBySortVo.getQueryRange();
        String[] split = queryRange.split("-");
        String firstPram = split[0];
        String lastPram = split[1];
        List<MarketInfo> shopBySortVo = marketMapper.findShopBySortVo(findShopBySortVo.getFloor(), findShopBySortVo.getBuildId());
        for (MarketInfo marketInfo:shopBySortVo){
            String transliteration = marketInfo.getTransliteration();
            if(transliteration.length()>=1){
                String matchChar = transliteration.substring(0, 1).toUpperCase();
                String matchAZ = matchAZ(firstPram, lastPram);
                if(matchAZ != null){
                    if(matchAZ.contains(matchChar)){
                        MarketDetailDto marketDetailDto = MarketTrams.INSTANCE.toMarketDetailDto(marketInfo);
                        marketDetailDtos.add(marketDetailDto);
                    }
                }else {
                    return CommonResult.failed("输入范围无法匹配字符A-Z");
                }
            }else {
                return CommonResult.failed("数据库音译错误");
            }
        }
        PageBean<MarketDetailDto> mapManagerDtoPageBean = restPage(marketDetailDtos);
        return CommonResult.success(mapManagerDtoPageBean);
    }

    /**
     * 匹配A-Z字符串
     * @param firstPram
     * @param lastPram
     * @return {@link String}
     */
    private String matchAZ(String firstPram,String lastPram){
        Integer firstPramIndex = Integer.valueOf(A_Z.toUpperCase().indexOf(firstPram.toUpperCase()));
        Integer lastPramIndex = Integer.valueOf(A_Z.toUpperCase().indexOf(lastPram.toUpperCase()));
        if(!firstPramIndex.equals(-1) && !lastPramIndex.equals(-1)){
            String substring = A_Z.substring(firstPramIndex, lastPramIndex+1);
            return substring;
        }else {
            return null;
        }
    }

    @Override
    public CommonResult<PageBean> findShopByCommercialType(findShopByCommercialTypeVo findShopByCommercialTypeVo) {
        String commercialType = findShopByCommercialTypeVo.getCommercialType();
        String floor = findShopByCommercialTypeVo.getFloor();
        Integer currentPage = findShopByCommercialTypeVo.getCurrentPage();
        Integer pageSize = findShopByCommercialTypeVo.getPageSize();

        ArrayList<MarketDetailDto> marketDetailDtos = new ArrayList<>();
        if(currentPage != null && pageSize != null){
            PageHelper.startPage(currentPage,pageSize);
        }

        List<MarketInfo> shopByCommercialType = marketMapper.findShopByCommercialType(commercialType, floor,findShopByCommercialTypeVo.getBuildId());
        for (MarketInfo marketInfo:shopByCommercialType){
            MarketDetailDto marketDetailDto = MarketTrams.INSTANCE.toMarketDetailDto(marketInfo);
            marketDetailDtos.add(marketDetailDto);
        }
        PageBean<MarketDetailDto> mapManagerDtoPageBean = restPage(marketDetailDtos);
        return CommonResult.success(mapManagerDtoPageBean);
    }

    @Override
    public CommonResult<NavigateResultDTO> getRoutePlanning(RoutePlanningVo routePlanning) {
        return CommonResult.success();
    }




    @Override
    public CommonResult<List<String>> getAllFloor(String buildId) {
        List<String> allFloor = marketMapper.findAllFloor(buildId);
        HashSet<String> bSet = new HashSet<>();
        HashSet<String> lSet = new HashSet<>();

        for(String floor:allFloor){
            if(floor.contains("L")){
                lSet.add(floor);
            } else if (floor.contains("B")) {
                bSet.add(floor);
            }
        }
        //楼上正序排
        ArrayList<String> lArray = new ArrayList<>(lSet);
        String[] floorArr = lArray.toArray(new String[0]);
        Arrays.sort(floorArr);
        //楼下倒叙
        ArrayList<String> bArray = new ArrayList<>(bSet);
        String[] underArr = bArray.toArray(new String[0]);
        Arrays.sort(underArr,Comparator.reverseOrder());

        String[] strings1 = Arrays.copyOf(underArr, underArr.length + floorArr.length);
        System.arraycopy(floorArr, 0, strings1, underArr.length, floorArr.length);

        List<String> result = Arrays.asList(strings1);


        return CommonResult.success(result) ;
    }

    @Override
    public CommonResult<List<CommercialTypeInfoDto>> getAllCommercialType(String buildId) {
        List<CommercialTypeInfo> allCommercialType = commercialTypeInfoMapper.getAllCommercialType(buildId);
        ArrayList<CommercialTypeInfoDto> commercialTypeInfoDtos = new ArrayList<>();
        for (CommercialTypeInfo commercialTypeInfo:allCommercialType){
            CommercialTypeInfoDto commercialTypeInfoDto = CommercialTypeTrans.INSTANCE.toCommercialTypeInfoDto(commercialTypeInfo);
            commercialTypeInfoDtos.add(commercialTypeInfoDto);
        }
        return CommonResult.success(commercialTypeInfoDtos) ;
    }

    @Override
    public CommonResult<PageBean> getMarketByName(getMarketByNameVo getMarketByNameVo) {
        String mapName = getMarketByNameVo.getMapName();
        String buildId = getMarketByNameVo.getBuildId();
        ArrayList<MarketDetailDto> marketDetailDtos = new ArrayList<>();
        List<MarketInfo> marketByName = marketMapper.getMarketByName(mapName,buildId);
        for (MarketInfo marketInfo:marketByName){
            MarketDetailDto marketDetailDto = MarketTrams.INSTANCE.toMarketDetailDto(marketInfo);
            marketDetailDtos.add(marketDetailDto);
        }
        PageBean<MarketDetailDto> mapManagerDtoPageBean = restPage(marketDetailDtos);
        return  CommonResult.success(mapManagerDtoPageBean);
    }

    @Override
    public CommonResult<MarketDocDto>getPictureById(getPictureByIdVo getPictureByIdVo) {
        String marketId = getPictureByIdVo.getMarketId();
        String buildId = getPictureByIdVo.getBuildId();
        String pictureType = getPictureByIdVo.getPictureType();
        MarketDoc pictureById = null;
        if(pictureType.equals(NavigatorConstants.highQuality)){
             pictureById = marketDocMapper.getHighPictureById(marketId,buildId);
        }else if (pictureType.equals(NavigatorConstants.lowQuality)){
             pictureById = marketDocMapper.getLowPictureById(marketId,buildId);
        }
        MarketDocDto marketDocDto = MarketDocTrans.INSTANCE.toMarketDocDto(pictureById);
        return CommonResult.success(marketDocDto);
    }

    @Override
    public byte[] getModelByFloor(getModelByFloorVo getFloorVo) throws IOException {
        String buildId = getFloorVo.getBuildId();
        String floor = getFloorVo.getFloor();
        byte[] bytes = null;
        File file1 = new File("");
        File absoluteFile = file1.getAbsoluteFile();
        logger.info("当前文件夹为：{}",absoluteFile.getPath());
        if(!("".equals(buildId)&&"".equals(floor))){
            ModelDoc modelDocByBuildAndFloor = modelDocMapper.getModelDocByBuildAndFloor(buildId, floor);
            if(modelDocByBuildAndFloor != null){
                String folder = modelDocByBuildAndFloor.getFolder();
                String targetFolder = absoluteFile.getPath()+MODEL_FILE + folder;
                logger.info("目标文件夹为：{}",targetFolder);
                File target = new File(targetFolder);
                if (target.isDirectory()){
                    File[] files = target.listFiles();
                    for (File file : files){
                        logger.info("文件有绝对路径：{}",file.getAbsoluteFile().getPath());
                        logger.info("文件相对路径：{}",file.getPath().toString());
                        System.out.println(file);
                        bytes = glbToBinary(file.getPath());
                    }
                }
            }
        }
        return bytes;
    }

    public byte[] glbToBinary(String modelPath) throws IOException {

        FileInputStream fis = new FileInputStream(modelPath);

        // 读取GLB文件内容
        byte[] glbBytes = Files.readAllBytes(Paths.get(modelPath));//@@ jdk8处理方式
        fis.close();

//        // 压缩为二进制流
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        DeflaterOutputStream dos = new DeflaterOutputStream(baos);
//        dos.write(glbBytes);
//        dos.close();

        return glbBytes;
    }
    @Override
    public CommonResult<SimulationNavigateResultDTO> getSimulationRoutes(SimulationRouteVO simulationRouteVO) {
        String sourcePoint = "";
        MapPointInfo targetPoint = mapPointMapper.getPointByMarketId(simulationRouteVO.getTargetMarketId());
        if (Objects.equals(targetPoint,null)){
            return CommonResult.failed("获取目标点信息错误");
        }
        if (Objects.equals(simulationRouteVO.getSourcePointId(),null) ||
                simulationRouteVO.getSourcePointId().isEmpty()){
            sourcePoint = pc;
        }
        SimulationNavigateResultDTO routePlaning = dijkstraRouter.getRoutePlaning(sourcePoint, targetPoint.getPointID());
        if (Objects.equals(routePlaning,null)){
            return CommonResult.failed("未找到相关路线，请检查点位");
        }
        return CommonResult.success(routePlaning);
    }


}
