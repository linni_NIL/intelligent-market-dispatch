package team.id.market.setting.controller;

import team.id.market.common.result.CommonResult;
import team.id.market.setting.entity.dto.ChargeInfoDTO;
import team.id.market.setting.entity.vo.ChargeInfoVO;
import team.id.market.setting.service.ChargeInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController("ChargeController")
@RequestMapping("/pc/setting/charge/manager")
@Api(tags = "充电设置")
public class ChargeInfoController {
    @Autowired
    private ChargeInfoService chargeInfoService;


    @GetMapping("/findChargeConfig")
    @ApiOperation(value = "查看充电参数配置")
    public CommonResult<ChargeInfoDTO> findChargeConfig() {
        return CommonResult.success(chargeInfoService.findChargeConfig());
    }

    @PostMapping("/updateChargeConfig")
    @ApiOperation(value = "修改充电参数配置")
    public CommonResult<String> updateChargeConfig(@RequestBody @Valid ChargeInfoVO chargeInfo) {
        return chargeInfoService.updateChargeConfig(chargeInfo);
    }

    @PostMapping("/enableCharge")
    @ApiOperation(value = "是否开启充电")
    public CommonResult<String> updateChargeConfig(@RequestParam @NotNull Integer isEnable, @RequestParam @NotNull Integer chargeId) {
        return chargeInfoService.enableCharge(isEnable, chargeId);
    }

}
