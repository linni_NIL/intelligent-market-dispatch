package team.id.market.setting.controller;

import team.id.market.common.result.CommonResult;
import team.id.market.setting.entity.dto.MachineCodeInfo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author 431587
 * @date 2023/5/30 14:13
 */

@RestController("系统登录")
@RequestMapping("/pc/login")
public class SystemLogin {
    @Autowired
    private team.id.market.setting.service.systemService systemService;


    @GetMapping("/getMachineCode")
    @ApiOperation(value = "获取机器码")
    public CommonResult<MachineCodeInfo> getMachineCode(){
        MachineCodeInfo machineCode = systemService.getMachineCode();
        return CommonResult.success(machineCode);
    }

    @PostMapping("/password")
    @ApiOperation(value = "登录")
    public CommonResult<MachineCodeInfo> login(@RequestParam String verifyCode){
        //校验islogin
        return systemService.login(verifyCode);

    }

    @PostMapping("/kernelLogin")
    @ApiOperation(value = "内核连接")
    public CommonResult<MachineCodeInfo> kernelLogin(){
        return null;
    }

    @PostMapping("/initLoadMap")
    @ApiOperation(value = "尝试加载地图")
    public CommonResult<String> initLoadMap(){

        return null;
    }







}
