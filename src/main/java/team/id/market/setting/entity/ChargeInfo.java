package team.id.market.setting.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChargeInfo {
    private Integer id;
    private Integer chargeId;
    private Integer energyLevelCritical;
    private Integer energyLevelGood;
    private Integer energyLevelFully;
    private Integer isEnable;
}
