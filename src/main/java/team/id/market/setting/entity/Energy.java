package team.id.market.setting.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Energy {
//    危险电量
    private Integer energyLevelCritical;
    private Integer energyLevelGood;
    private Integer energyLevelFully;
    private Integer isEnable;
}
