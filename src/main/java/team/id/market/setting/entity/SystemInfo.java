package team.id.market.setting.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author 431587
 * @date 2023/6/29 8:47
 */
@Data
public class SystemInfo {

    private Integer id;

    private Integer kernelLinkStatus;

    private String machineCode;

    private String licence;

    private Date createTime;
}
