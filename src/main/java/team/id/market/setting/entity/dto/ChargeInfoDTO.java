package team.id.market.setting.entity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChargeInfoDTO {
    private Integer chargeId;
    private Integer energyLevelCritical;
    private Integer energyLevelGood;
    private Integer energyLevelFully;
    private Integer isEnable;
}
