package team.id.market.setting.entity.dto;

import lombok.Data;

/**
 * @author 431587
 * @date 2023/5/30 14:15
 */

@Data
public class MachineCodeInfo {

    /*机器码*/
    private  String machineCode;

    /*是否登录*/
    private Boolean isLogin;


}
