package team.id.market.setting.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChargeInfoVO {
    @NotNull(message = "充电配置id不允许为空")
    private Integer chargeId;
    @NotNull(message = "危险电量不允许为空")
    @Max(value = 100,message = "电量最大值不得超过100")
    @Min(value = 1,message = "电量最大值不得低于1")
    private Integer energyLevelCritical;
    @NotNull(message = "充满电量不允许为空")
    @Max(value = 100,message = "电量最大值不得超过100")
    @Min(value = 1,message = "电量最大值不得低于1")
    private Integer energyLevelGood;
    @NotNull(message = "满电阈值不允许为空")
    @Max(value = 100,message = "电量最大值不得超过100")
    @Min(value = 1,message = "电量最大值不得低于1")
    private Integer energyLevelFully;
}
