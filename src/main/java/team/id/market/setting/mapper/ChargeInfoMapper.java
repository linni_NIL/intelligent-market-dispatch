package team.id.market.setting.mapper;

import team.id.market.setting.entity.ChargeInfo;
import team.id.market.setting.entity.dto.ChargeInfoDTO;
import team.id.market.setting.entity.vo.ChargeInfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ChargeInfoMapper {

    List<ChargeInfo> findChargeConfig();

    void initChargeConfig(ChargeInfoDTO chargeInfoDTO);

    void enableCharge(@Param("isEnable") Integer isEnable, @Param("chargeId") Integer chargeId);

    void updateChargeConfig(ChargeInfoVO chargeInfoVO);

}
