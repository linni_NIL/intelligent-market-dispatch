package team.id.market.setting.mapper;

import team.id.market.setting.entity.SystemInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author 431587
 * @date 2023/6/28 11:44
 */
@Mapper
public interface SystemMapper {

    List<SystemInfo> getSystemInfo();

    void insertSystemInfo(@Param("createTime") Date createTime);

    void updateKernelStautsById(@Param("kernelStatus") Integer kenelStatus, @Param("id") Integer id);

    void updateSystemInfo(SystemInfo systemInfo);

}
