package team.id.market.setting.service;

import team.id.market.common.result.CommonResult;
import team.id.market.setting.entity.dto.ChargeInfoDTO;
import team.id.market.setting.entity.vo.ChargeInfoVO;

public interface ChargeInfoService {
   ChargeInfoDTO findChargeConfig();

   CommonResult<String> enableCharge(Integer isEnable, Integer chargeId);

   CommonResult<String> updateChargeConfig(ChargeInfoVO chargeInfoVO);
}
