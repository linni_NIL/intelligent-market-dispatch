package team.id.market.setting.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import team.id.market.common.exception.MessageException;
import team.id.market.common.result.CommonResult;
import team.id.market.common.result.Result;
import team.id.market.common.utils.HttpUtil;
import team.id.market.setting.entity.ChargeInfo;
import team.id.market.setting.entity.Energy;
import team.id.market.setting.entity.SystemInfo;
import team.id.market.setting.entity.dto.ChargeInfoDTO;
import team.id.market.setting.entity.vo.ChargeInfoVO;
import team.id.market.setting.mapper.ChargeInfoMapper;
import team.id.market.setting.mapper.SystemMapper;
import team.id.market.setting.service.ChargeInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static team.id.market.common.utils.HttpUtil.RESPONSE_SUCCESS;

@Service
public class ChargeInfoServiceImpl implements ChargeInfoService {

    @Autowired
    private ChargeInfoMapper chargeInfoMapper;

    @Autowired
    private SystemMapper systemMapper;


    private ObjectMapper mapper = new ObjectMapper();

    private Map<String, String> requestHeader;

    private Gson gson = new Gson();

    public ChargeInfoServiceImpl() {
        requestHeader = new HashMap<>();
        requestHeader.put("Cookie", "");
        requestHeader.put("Connection", "keep-alive");
        requestHeader.put("Accept", "*/*");
        requestHeader.put("Accept-Language", "zh-CN,zh;q=0.9");
        requestHeader.put("Accept-Encoding", "gzip, deflate, br");
        requestHeader.put("User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"
        );
    }


    private static final Logger logger = LoggerFactory.getLogger(ChargeInfoServiceImpl.class);

    @Override
    public ChargeInfoDTO findChargeConfig() {
        List<ChargeInfo> databaseChargeConfiguration = chargeInfoMapper.findChargeConfig();

        if (databaseChargeConfiguration.isEmpty()) {
            logger.info("数据库无充电配置，默认返回配置数据");
            ChargeInfoDTO chargeInfoDTO = new ChargeInfoDTO(1, 5, 20, 90, 0);
            chargeInfoMapper.initChargeConfig(chargeInfoDTO);
            return chargeInfoDTO;
        }

        ChargeInfoDTO chargeInfo = new ChargeInfoDTO();
        chargeInfo.setChargeId(databaseChargeConfiguration.get(0).getChargeId());
        chargeInfo.setEnergyLevelCritical(databaseChargeConfiguration.get(0).getEnergyLevelCritical());
        chargeInfo.setEnergyLevelGood(databaseChargeConfiguration.get(0).getEnergyLevelGood());
        chargeInfo.setEnergyLevelFully(databaseChargeConfiguration.get(0).getEnergyLevelFully());
        chargeInfo.setIsEnable(databaseChargeConfiguration.get(0).getIsEnable());
        return chargeInfo;
    }


    @Override
    @Transactional
    public CommonResult<String> enableCharge(Integer isEnable, Integer chargeId) {
        return null;
    }

    @Override
    @Transactional
    public CommonResult<String> updateChargeConfig(ChargeInfoVO chargeInfoVO) {
        SystemInfo systemMInfo = systemMapper.getSystemInfo().get(0);
        Energy energy = new Energy();
        energy.setEnergyLevelCritical(chargeInfoVO.getEnergyLevelCritical());
        energy.setEnergyLevelGood(chargeInfoVO.getEnergyLevelGood());
        energy.setEnergyLevelFully(chargeInfoVO.getEnergyLevelFully());
        String jsonData = gson.toJson(energy);
        String requestURL = HttpUtil.doPost("http://127.0.0.1:55200/v1/energy/updateEnergy", requestHeader, jsonData);
        try {
            Result result = mapper.readValue(requestURL, Result.class);
            logger.info("接口{}返回数据{}", requestURL, result.toString());
            if (result.isSuccess()) {
//              内核成功修改后再同步数据库
                chargeInfoMapper.updateChargeConfig(chargeInfoVO);
                return CommonResult.success();
            } else {
                return CommonResult.failed();
            }
        } catch (IOException e) {
            logger.error("接口{}错误，信息为{}", requestURL, e.toString());
            return CommonResult.failed();
        }
    }
}
