package team.id.market.setting.service.impl;

import cn.hutool.core.date.DateUtil;
import team.id.market.common.result.CommonResult;
import team.id.market.common.utils.SerialNumberUtil;
import team.id.market.setting.entity.SystemInfo;
import team.id.market.setting.entity.dto.MachineCodeInfo;
import team.id.market.setting.mapper.SystemMapper;
import team.id.market.setting.service.systemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.interfaces.RSAPrivateKey;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 431587
 * @date 2023/7/18 14:34
 */
@Service("systemServiceImpl")
public class systemServiceImpl implements systemService {
    private static String regex = "(?i)[a-z0-9_+-]+";
    @Autowired
    SystemMapper systemMapper;

    @Override
    public MachineCodeInfo getMachineCode() {
        //检查是否有数据
        List<SystemInfo> systemInfo1 = systemMapper.getSystemInfo();
        if(systemInfo1.size() == 0){
            systemMapper.insertSystemInfo(DateUtil.date());
        }
        SystemInfo systemInfo = systemMapper.getSystemInfo().get(0);
        String machineCode = systemInfo.getMachineCode();
        if(machineCode == null || Objects.equals(machineCode,"")){
            machineCode = SerialNumberUtil.machineCode();
            SystemInfo systemInfo2 = new SystemInfo();
            systemInfo2.setMachineCode(machineCode);
            systemInfo2.setId(systemInfo.getId());
            systemMapper.updateSystemInfo(systemInfo2);
        }
        MachineCodeInfo machineCodeInfo = new MachineCodeInfo();
        machineCodeInfo.setMachineCode(machineCode);
        machineCodeInfo.setIsLogin(false);
        return machineCodeInfo;
    }

    @Override
    public CommonResult<MachineCodeInfo>  login(String encryptText) {

        MachineCodeInfo machineCodeInfo = new MachineCodeInfo();
        SystemInfo systemInfo = systemMapper.getSystemInfo().get(0);
        if(Objects.equals(encryptText, "")){
            encryptText = systemInfo.getLicence();
        }
        if(!Objects.equals(systemInfo,null)){
            if(!Objects.equals(encryptText,null) && !Objects.equals(encryptText,"") ){
                encryptText= encryptText.replaceAll(" ","+");

                Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(encryptText);
                if(!matcher.matches()){
                    return CommonResult.failed("校验错误，请输入正确License");
                }
                RSAPrivateKey privateKey = SerialNumberUtil.getPrivateKey(SerialNumberUtil.privateKey);
                String decryptText = SerialNumberUtil.decrypt(encryptText,privateKey);
                String machineCode = systemInfo.getMachineCode();
                if(Objects.equals(decryptText,machineCode)){
                    systemInfo.setLicence(encryptText);
//                    systemMapper.updateSystemInfo(systemInfo);
                    machineCodeInfo.setIsLogin(true);
                }else {
                    machineCodeInfo.setIsLogin(false);
                    return CommonResult.failed("校验错误，请输入正确License");
                }
            }else {
                return CommonResult.failed("系统错误，请添加License");
            }
        }
        else {
            return CommonResult.failed("系统错误，请添加系统信息");
        }
        return CommonResult.success(machineCodeInfo) ;
    }
}
