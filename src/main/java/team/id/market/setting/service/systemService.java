package team.id.market.setting.service;

import team.id.market.common.result.CommonResult;
import team.id.market.setting.entity.dto.MachineCodeInfo;

/**
 * @author 431587
 * @date 2023/7/18 14:33
 */
public interface systemService {

    MachineCodeInfo getMachineCode();

    CommonResult<MachineCodeInfo> login(String code);
}
