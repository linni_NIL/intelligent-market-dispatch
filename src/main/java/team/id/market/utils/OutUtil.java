package team.id.market.utils;

import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

public class OutUtil {

    private static String charsetName = "utf-8";

    private static String getFileContentType(String name) {
        String s = "";
        String n = name.toLowerCase();
        if (n.endsWith("png")) {
            s = "image/png";
        } else if (n.endsWith("gif")) {
            s = "image/gif";
        } else if (n.endsWith("jpg") || n.endsWith("jpeg")) {
            s = "image/jpeg";
        } else if(n.endsWith("svg")){
            s = "image/svg+xml";
        }else if (n.endsWith("doc")) {
            s = "application/msword";
        } else if (n.endsWith("xls")) {
            s = "application/x-excel";
        } else if (n.endsWith("zip")) {
            s = "application/zip";
        } else if (n.endsWith("pdf")) {
            s = "application/pdf";
        } else {
            s = "application/octet-stream";
        }
        return s;
    }

    /**
     * 按照字节输出文件流
     * @param bytes
     * @param resp
     * @param type
     */
    public static void outStreamBytes(byte[] bytes, HttpServletResponse resp, String type){
        OutputStream os = null;
        try {
            resp.setContentType(getFileContentType(type)+"; charset=" + charsetName);
            os = resp.getOutputStream();
            os.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != os){
                    os.close();
                }
            } catch (IOException e) {
            }
        }
    }

    public static void outStreamBytes1(byte[] bytes, HttpServletResponse resp){
        OutputStream os = null;
        try {
//            resp.setContentType(getFileContentType(type)+"; charset=" + charsetName);
            os = resp.getOutputStream();
            os.write(bytes);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != os){
                    os.close();
                }
            } catch (IOException e) {
            }
        }
    }
    /**
     * 基础字节数组输出
     */
    private static void out(InputStream is, OutputStream os) {
        try {
            byte[] buffer = new byte[10240];
            int length = -1;
            while ((length = is.read(buffer)) != -1) {
                os.write(buffer, 0, length);
                os.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
